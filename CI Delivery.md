# Deployment Process
## 1. Environment Checklist
### 1.1. Facebook Page Checklist
|No.|Items                                                  |Production Data    |
|---|-------------------------------------------------------|-------------------|
|1  |Having the Facebook Page Name                          |                   |
|2  |Having the Facebook Page Type                          |                   |
|3  |Having the Facebook avatar photo                       | assets/images/fb/avatar.png|
|4  |Having the Facebook cover photo                        | assets/images/fb/cover.png|
|5  |Update the Messenger Greeting message                  |Chào [name]! Bạn có thắc mắc và cần bạn đồng hành giải đáp? Hãy nhấn vào nút BẮT ĐẦU/GET STARTED để chat với [BotName] nhé!|

### 1.2. Facebook App Checklist
|No.|Items                                                  |Production Data    |
|---|-------------------------------------------------------|-------------------|
|1  |Having the Facebook App Icon                           | assets/images/fb/app_icon.png|
|2  |Having the Privacy Policy URL                          |                   |
|3  |Having `AWS Server URL (Webhook URL)`                  |                   |
|4  |Having `FB_PAGE_ACCESS_TOKEN`                          |                   |
|5  |Turn the Page to Public                                |                   |

### 1.3. API.ai Checklist
|No.|Items                                                  |Production Data    |
|---|-------------------------------------------------------|-------------------|
|1  |Having the API.ai account                              |Username/Password: |
|2  |Having the Hosting information                         |Host/port:         |
|3  |Having the `APIAI_ACCESS_TOKEN`                        |                   |
|4  |Having the `FB_VERIFY_TOKEN`                           |                   |
|5  |Having the Agent                                       |                   |
|6  |Having the Intents import zip file                     | /assets/api.ai/intents.zip                   |

### 1.4. AWS Server Checklist
|No.|Items                                  |Production data        |
|---|---------------------------------------|-----------------------|
|1  |Having the Amazon Account              |username/password:     |
|2  |Upgrade Ubutun Package                 |sudo apt-get update && sudo apt-get upgrade       |
|3  |Install NodeJS                         |sudo apt-get install nodejs|
|4  |Upgrade npm                            |sudo npm install -g npm@latest|
|5  |check version node                     |node --version         |
|6  |check version npm                      |npm --version          |
|7  |Import the key for the official MongoDB repository|sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80|
|8  |Create a list file for MongoDB         |sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list|
|9  |Update the packages list               |sudo apt-get update    |
|10 |Install mongodb-org                    |sudo apt-get install mongodb-org|
|11 |Restart MongoDB                        |sudo systemctl enable mongod|
|12 |Enable UFW   |                         |sudo ufw enable        |
|13 |Set the default MongoDB port           |sudo ufw allow 27017   |
|14  |Configure a Public BindIP             |bindIP:                |
|15 |Test the Remote Connection             |mongo --host ip_address|

## 2. Production Process
### 2.1 Setting up Facebook Page 
To create a Facebook bot we need 2 things:

-   **Facebook public page** you’ll connect your bot to.
-   **Facebook Developer application** which will be connected to your webhook server and your public page and work as a middleware between them.

So, at first you need to create a page. Go to [Facebook Page Creation](http://www.facebook.com/pages/create). Choose a category, subcategory and click `Get Started`.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page_category.png)

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-type.png)

#### Update avatar and cover photo

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page.png)

1. Select the avatar photo
2. Select the cover photo
3. Setting up the `Get Started` button for Messenger

### Setting up the "Send Message" button for Messenger
- Click `Add button` button to set up the `Get Started` button for your Messenger
- Select `Get in Touch` option

![](https://bytebucket.org/SolutionPortal/production/raw/94cbd57446bc6bd9f5c83836afaca499e26aa1cb/assets/images/fb-developer/facebook-add-button-to-page.png)

- Select `Send Message` option

![](https://bytebucket.org/SolutionPortal/production/raw/94cbd57446bc6bd9f5c83836afaca499e26aa1cb/assets/images/fb-developer/facebook-add-get-started-button.png)

#### Update Messenger Greeting
- Go to `Settings`
- Click `Messaging`
- Turn on `Greeting Message` as `Yes`

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-settings.png)

- Click `Change` button to update message

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-setting-greeting.png)

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-messenger-greeting-2.png)

- Add Personlisation and `Save` button to update your Messenger Greeting.

After that you’ll need to create an app. 

### 2.2 Setting up Facebook Application
- Go to [Facebook Developer](https://developers.facebook.com/apps/), to create your Facebook App 

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-developer-app.png)

- Click button `Add a New App`

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-create_app.png)

- Fill `Display Name` and `Contact Email` -> Click button `Create App ID`
- Now your Facebook app is successfully created 

After creating an app click `Add Product` from the left menu, then choose `Messenger` and click `Set Up` button.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-app-messenger.png)

At the `Token Generation` section, choose the page you just created in order to generate a token. Facebook may request your permission before generating token. Don't worry, this process happens only once time, you just follow the instruction.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-get-Page%20Access%20Token.png)

#### Setting up Webhook
Click on `Setup Webhooks` button to fill the following information

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-app-setup-webhooks.png)

1. `Callback URL` with your **[AWS Server URL](https://chatbot.aws-server.com)**
2. `Verify Token`, basically it’s just a random string for validation. For example: chatbot-demo, chatbot-production
3. Check messages in the `Subcription Fields` section below.
	
![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-app-page-subscription.png)

Click `Verify and Save` button to complete the integration.

### 2.3 Setting up API.ai
#### Update the .env file
The following fields should be changed at the begining of deployment process:

- Host:
- Port:
- FB_VERIFY_TOKEN 
- FB_PAGE_ACCESS_TOKEN
- APIAI_ACCESS_TOKEN
    
![](https://bytebucket.org/SolutionPortal/production/raw/aef80af6be095e3d8b6a32b01723ba4e3f5a196a/assets/images/api.ai/update-env-file.png)

#### Import Agent
Agents can be described as NLU (Natural Language Understanding) modules for applications. Their purpose is to transform natural user language into actionable data and to manage a conversation flow.

- Click `Create Agent` button to create a new agent.

    ![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/api.ai/agent-list.png)

- Enter the name of your new agent, choose the language you want your app to communicate in.

    ![](https://bytebucket.org/SolutionPortal/production/raw/8201184511c5b94816fdef8696973432df773226/assets/images/api.ai/create-agent.png)

- Once you have named your agent and selected a language, click the `Save` button.

#### Import Intents
An intent determines what kind of user requests and the actionable response.

- Go to `Agent Settings` an click on `Export and Import` tab

    ![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/api.ai/settings.png)

- Select the intents.zip to upload. Noted that it will not delete the current ones. Intents and entities with the same name will be replaced with the newer version.

    ![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/api.ai/import-intent.png)
    
    ![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/api.ai/upload-agent.png)

- Now, your intents are successfully imported into the Agent
You can manually create new intent by clicking on `Create Intent` button

    ![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/api.ai/intent-list.png)

Here are the list of available intents

|No.|Intent Name        |Purpose                                            |
|---|-------------------|---------------------------------------------------|
|1  |Greetings          |Define the greetings scenario when users start conversation with Bot   |
|2  |About Corporation  |Define the chat introduction scenario              |
|3  |Lead Name          |Define the Name Confirmation/Asking scenario       |
|4  |Lead Email         |Define the Email Confirmation/Asking scenario      |
|5  |Lead Birthday      |Define the Birthday Confirmation/Asking scenario   |
|6  |Lead Gender        |Define the Gender Confirmation/Asking scenario     |
|7  |Current Location   |Define the Current Location Getting scenario       |

## 3. Testing Support
### 3.1 REST API Testing
### 3.2 API.ai Intent Testing
### 3.3 UI (Facebook Messenger) Testing
