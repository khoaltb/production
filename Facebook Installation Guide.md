# Facebook Installation Guideline
## 1. Setting up Facebook Page 
To create a Facebook bot we need 2 things:

-   **Facebook public page** you’ll connect your bot to.
-   **Facebook Developer application** which will be connected to your webhook server and your public page and work as a middleware between them.

So, at first you need to create a page. Go to [Facebook Page Creation](http://www.facebook.com/pages/create), then select Page Type

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page_category.png)

Next, choose a category, subcategory and click `Get Started`.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-type.png)

### Update avatar and cover photo

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page.png)

1. Select the avatar photo
2. Select the cover photo
3. Set up the `Send Message` button for your Facebook page.

### Setting up the "Send Message" button for Messenger
- Click `Add button` button to set up the `Get Started` button for your Messenger
- Select `Contact you`, then `Send Message` option and `Next`

![](https://bytebucket.org/SolutionPortal/production/raw/94cbd57446bc6bd9f5c83836afaca499e26aa1cb/assets/images/fb-developer/facebook-add-button-to-page.png)

Your Buton `Send Message` is set up.

![](https://bytebucket.org/SolutionPortal/production/raw/94cbd57446bc6bd9f5c83836afaca499e26aa1cb/assets/images/fb-developer/facebook-add-get-started-button.png)

### Update Messenger Greeting
- Go to `Settings`
- Click `Messaging`
- Turn on `Greeting Message` as `Yes`

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-settings.png)

- Click `Change` button to update message

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-page-setting-greeting.png)

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-messenger-greeting-2.png)

- Add Personlisation and `Save` button to update your Messenger Greeting.

After that you’ll need to create an app. 

## 2. Setting up Facebook Application
- Go to [Facebook Developer](https://developers.facebook.com/apps/), to create your Facebook App 

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-developer-app.png)

- Click button `Add a New App`

![](https://bytebucket.org/SolutionPortal/production/raw/fa37e53856831fbbd0d83b9011b71fdb338ca3fd/assets/images/fb-developer/facebook-create_app.png)

- Fill `Display Name` and `Contact Email` -> Click button `Create App ID`
- Now your Facebook app is successfully created 

## 3. Integrate chatbot to Facebook page
After creating an app click `Add Product` from the left menu, then choose `Messenger` and click `Set Up` button.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-app-messenger.png)

At the `Token Generation` section, choose the page you just created in order to generate a token. Facebook may request your permission before generating token. Don't worry, this process happens only once time, you just follow the instruction.

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-get-Page%20Access%20Token.png)

#### Setting up Webhook
Click on `Setup Webhooks` button to fill the following information

![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-app-setup-webhooks.png)

1. `Callback URL` with your **[AWS Server URL](https://chatbot.aws-server.com)**
2. `Verify Token`, basically it’s just a random string for validation. For example: chatbot-demo, chatbot-production
3. Check messages in the `Subcription Fields` section below.
	
![](https://bytebucket.org/SolutionPortal/production/raw/fa37e53856831fbbd0d83b9011b71fdb338ca3fd/assets/images/fb-developer/facebook-app-page-subscription.png)

Click `Verify and Save` button to complete the integration.

For item #1 and #2, you can go to Reply.ai to get `Callback URL` and `Verify Token`
    
![](https://bytebucket.org/SolutionPortal/production/raw/24d7610a2dff518ed84d649625f38e4e206a1a37/assets/images/fb-developer/reply-callback-url.png)


## 4. Submit for App Review
### Before App Submission 
There are a few things you'll want to take care of to make the process as easy as possible. The following fields are required before submitting

- App Icon: your icon should be a JPG, GIF or PNG file. The size of the image must be 1024 x 1024 pixels. File size limit 5 MB. 

- Long Description

- Privacy Policy

- Category: it should be `App for Messengers`.

![](https://bytebucket.org/SolutionPortal/production/raw/9d859bc295a53fd05792361854f7eb4f0b7c6897/assets/images/fb-developer/facebook-app-review.png)

### Submission
- Make your page is public, then `Start a Submission`

![](https://bytebucket.org/SolutionPortal/production/raw/9d859bc295a53fd05792361854f7eb4f0b7c6897/assets/images/fb-developer/make-page-public.png)

For more information, refer [How to Submit for Feature Review](https://developers.facebook.com/docs/facebook-login/review/how-to-submit)

## 5. Test your page
- Go to [https://developers.facebook.com/](https://developers.facebook.com/)
- Select `Roles` menu on the left side.
- Add FB ID of your testers

![](https://bytebucket.org/SolutionPortal/production/raw/9d859bc295a53fd05792361854f7eb4f0b7c6897/assets/images/fb-developer/fb-add-tester.png)

Now, your bot is ready to test.