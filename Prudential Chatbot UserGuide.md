# Scenarios
## 1. Current Customer
![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/userguide/GIF%20format/Current_Customer.gif)
## 2. Prospect
![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/userguide/GIF%20format/Prospect.gif)
## 3. Product Exploration
![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/userguide/GIF%20format/Product_Exploration.gif)
## 4. Promotion
![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/userguide/GIF%20format/Promotion.gif)
# Config Data
## 1. Mini Games
### How to turn on/off mini game
- Step 1: Log into Reply.ai
- Step 2: Go to `Content` menu on the left side.
    ![](https://bytebucket.org/SolutionPortal/production/raw/626962b58af280e1e6fa44f3e7c077d75498c5c2/assets/images/reply.ai/menu-content.png)
    Now, you are in `Content` page where you can modify the configurable parameters.
- Step 3: Find `ON_OFF_MINI_GAME` paramter and click on "Edit" icon.
    ![](https://bytebucket.org/SolutionPortal/production/raw/401a75e8670ad611aa865ab9b0268750a5c10b3b/assets/images/reply.ai/turn-on-off-minigame.png)
    Now, you are in `Edit Content` page
- Step 4: 
    -  Update the value as `ON` if you want to turn on the Minigames.
    -  Update the value as `OFF` if you want to turn off the Minigames.
    ![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/assets/images/reply.ai/update-minigame-content.png)
    - Then, click on `Save Changes` button to complete the update.

## 2. Promotion
### How to turn on/off a promotion
- Step 1: Log into Reply.ai
- Step 2: Go to `Content` menu on the left side.
    ![](https://bytebucket.org/SolutionPortal/production/raw/626962b58af280e1e6fa44f3e7c077d75498c5c2/assets/images/reply.ai/menu-content.png)
    Now, you are in "Content" page where you can modify the configurable parameters.
- Step 3: Find `ON_OFF_PROMOTION` paramter and click on "Edit" icon.
    ![](https://bytebucket.org/SolutionPortal/production/raw/7ab02c0078deced9bd35a9183b280d42c9c506c0/assets/images/reply.ai/turn-on-off-promotion.png)
Now, you are in `Edit Content` page
- Step 4: 
    -  Update the value as `ON` if you want to turn on the Promotion.
    -  Update the value as `OFF` if you want to turn off the Promotion.
    ![](https://bytebucket.org/SolutionPortal/production/raw/7d275875405ce52e27a83fef5f62b777a3884bfc/assets/images/reply.ai/update-promotion-content.png)
    - Then, click on `Save Changes` button to complete the update.

### How to update the promotion content
You can add more promotion or update the copy of promotion.
- Step 1: Open the Promotion excel file
- Step 2: The content is structured as below:
    ![](https://bytebucket.org/SolutionPortal/production/raw/7d275875405ce52e27a83fef5f62b777a3884bfc/assets/images/reply.ai/update-promotion-detail.png)
**Noted**: Chat support up to **3** promotions at the same time
- Step 3: Copy the generated text and paste to the editor as a json file
- Step 4: Save this file as `promotion.json` file
- Step 5: Upload this file to the **source/public/promotion/** folder

### Add more promotion
- Step 1: Open the Promotion excel file
- Step 2: Add new promotion as below
    ![](https://bytebucket.org/SolutionPortal/production/raw/7d275875405ce52e27a83fef5f62b777a3884bfc/assets/images/add-new-promotion.gif)

### Update copy
- Step 1: Open the Promotion excel file
- Step 2: Add new promotion as below
    ![](https://bytebucket.org/SolutionPortal/production/raw/7d275875405ce52e27a83fef5f62b777a3884bfc/assets/images/update-promotion-content.gif)

# 2. Product
## How to turn on/off a product

## How to edit/update a product information
- Step 1: Open the Promotion excel file
- Step 2: The content is structured as below:
    ![]()
- Step 3: Copy the generated text and paste to the editor as a json file
    ![]()
- Step 4: Save this file as `product.json` file
- Step 5: Upload this file to the **source/public/product/** folder
# 3. Report 

