### Directory Structure

```
.
├── assets
│   ├── images
│   │   ├── api.ai
│   │   ├── fb-developer	
│   │   ├── reply.ai											
├── projects
│   ├── generali
│   ├── prudential
│   │   ├── assets
│   │   │   ├── images	
│   │   │   ├── reply.ai										<- imported file for reply.ai
│	│	│   │   ├── build	
│	│	│   │   │   ├── content									=> JSON: all configurable parameters in reply
│	│	│   │   │   ├── flows									=> JSON: all workflows and triggers in reply
│   │   ├── source												<- Source code of Chatbot
│   │   │   ├── Node.js											<- Nodejs source code
│   │   ├── userguide											<- VIDEO: how to chat with bot
│   │   │   ├── GIF format										=> GIF: all video in GIF format	
│   │   │   ├── MP4 format										=> MP4: all video in MP4 format	
├── Facebook Installation Guide.md								=> Markdown: Facebook Installation Document Guide
├── Prudential Chatbot UserGuide.md								=> Markdown: Userguide Document
├── Prudential+Production+Checklist.doc							=> Word: Production Checklist Document
├── README.md
├── Reply.AI Deployment Guide.md								=> Markdown: Reply.AI Deployment Document Guide

```