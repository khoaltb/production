# GUIDE TO DEPLOY SOURCE TO REPLY.AI
### I. Activate the Reply account
### II.  Deploy the flows into Reply 
### III.  Configure the parameters of Reply
### IV. Connect AWS server to Reply.ai
### V.  Configure Greeting Message
### VI. Configure Persistent Menu
---------------------------------------------------
## I. Activate the Reply account
- You will receive an invitation from Reply.ai to activate your account.

    ![](https://bytebucket.org/SolutionPortal/production/raw/85bd6a6faa515417eaf947a7239be996c65f6794/assets/images/reply.ai/invitation-email.png)

- Click on the link to activate your account.

## II. Deploy the flows into Reply
1. Log into Reply.ai
2. Select `Flows` menu on the left side.
3. Open the gear icon on the right corner and click `Import`

    ![Import Flow Menu](https://bytebucket.org/SolutionPortal/production/raw/401a75e8670ad611aa865ab9b0268750a5c10b3b/assets/images/reply.ai/import-flow-menu.png)

4. Select **Choose File -> Browse to flow Json file -> Import**
The import file template is available at [production/assets/reply.ai/build/flows](https://bitbucket.org/SolutionPortal/production/src/419f4ec0664e515b68d28fa55b6cc3a298877521/assets/reply.ai/build/flows)

    ![Import flows](https://bytebucket.org/SolutionPortal/production/raw/fd35a440be7a8f691dbefe7aaea7ceadb422592d/assets/images/reply.ai/reply-import-flow.png)

Now, the flow is successfully imported.
    ![Import Flow](https://bytebucket.org/SolutionPortal/production/raw/fd35a440be7a8f691dbefe7aaea7ceadb422592d/assets/images/reply.ai/import-flow-successfully.png)

## III. Deploy the content into Reply
1. Select `Content` menu on the left side.
2. Open the gear icon on the right corner and click `Import`

    ![Import Content Menu](https://bytebucket.org/SolutionPortal/production/raw/401a75e8670ad611aa865ab9b0268750a5c10b3b/assets/images/reply.ai/import-content-menu.png)
3. Select **Choose File -> Browse to content Json file -> Import**
The import file template is available at [production/assets/reply.ai/build/content](https://bitbucket.org/SolutionPortal/production/src/419f4ec0664e515b68d28fa55b6cc3a298877521/assets/reply.ai/build/content)

    ![Import Content](https://bytebucket.org/SolutionPortal/production/raw/fd35a440be7a8f691dbefe7aaea7ceadb422592d/assets/images/reply.ai/reply-import-content.png)

Now, the content is successfully imported.
    ![Import Content](https://bytebucket.org/SolutionPortal/production/raw/5735a6f2c06553ad2fd6fa467833865af3ad7117/assets/images/reply.ai/import-content-successfully.png)

## IV. Connect AWS server to Reply.ai
1. Select `Content` menu on the left side.
2. Select `api_base` parameter and click `Edit` icon

    ![Select API_BASE parameter](https://bytebucket.org/SolutionPortal/production/raw/650c8a15c34d783cecc7fdd066960bcc0d503222/projects/prudential/portfolio/assets/images/reply.ai/configure-api.png)

3. Fulfill the value by your AWS information and click `Save Changes` button

    ![API_BASE](https://bytebucket.org/SolutionPortal/production/raw/401a75e8670ad611aa865ab9b0268750a5c10b3b/assets/images/reply.ai/api-base-update-content.png)

## V. Integrate with Facebook Page
1. Select `Integration` menu on the left side.

    ![Integration](https://bytebucket.org/SolutionPortal/production/raw/6a5f0e1f8552df005682bb5476e9940753c79c11/assets/images/reply.ai/reply-connect-facebook.png)

2. Click `Connect` Facebook button 

![Integration](https://bytebucket.org/SolutionPortal/production/raw/6a5f0e1f8552df005682bb5476e9940753c79c11/assets/images/reply.ai/reply-connect-facebook-2.png)

`Page Access Token` and `App secret` can be get from [Facebook Developer page](https://developers.facebook.com/apps/) 
    - Page Acess Token
    
![](https://bytebucket.org/SolutionPortal/production/raw/15aa17a781a645541cdcffff1fc348274d41d229/assets/images/fb-developer/facebook-get-Page%20Access%20Token.png)
    - App Secret

![](https://bytebucket.org/SolutionPortal/production/raw/6a99f8d2cd6db75c4f4624e826e6c8951e07918f/assets/images/fb-developer/facebook-app-dashboard.png)

3. Click `Submit` to complete the Facebook Integration flow. 

## VI. Configure Greeting Message & Persistent Menu
1. Go to `Integration` section, then select `Facebook` and click `Settings` button

    ![](https://bytebucket.org/SolutionPortal/production/raw/6a99f8d2cd6db75c4f4624e826e6c8951e07918f/assets/images/reply.ai/reply-integration.png)
2. Go to `Messenger configuration` section and click `Bot Configuration` button

    ![](https://bytebucket.org/SolutionPortal/production/raw/4fd05874510a3c6a89a14176017198bbde71154b/assets/images/reply.ai/bot-configuration.png)
    
### Configure `Get Started` button
- Select `Greeting text` option 

![Get Started](https://bytebucket.org/SolutionPortal/production/raw/55122e107cdae1234b1660557729c023e0bd140b/assets/images/reply.ai/reply-config-get-started.png)

- Enter `menu` and click `Save Changes` button

### Update Greeting text
- Select `Greeting text` option 
- Paste the following text 
```
[
  {
    "text": "Chào {{user_full_name}}! Bạn có thắc mắc và cần bạn đồng hành giải đáp? Hãy nhấn vào nút BẮT ĐẦU/GET STARTED để chat với Prudential nhé!",
    "locale": "default"
  }
]
```

![Update Greeting Text](https://bytebucket.org/SolutionPortal/production/raw/55122e107cdae1234b1660557729c023e0bd140b/assets/images/reply.ai/update-greeting-text.png)

- And click `Save Changes` button
- Your greeting message will be shown as

    ![Greeting Message](https://bytebucket.org/SolutionPortal/production/raw/4fd05874510a3c6a89a14176017198bbde71154b/assets/images/reply.ai/fb-greeting-message.png)

### Update Persistent Menu
- Select `Persistent Menu` option 
- Paste the following text 
```
[
  {
    "call_to_actions": [
      {
        "payload": "toi muon hoi",
        "type": "postback",
        "title": "Tôi muốn hỏi"
      },
      {
        "payload": "tim hieu san pham",
        "type": "postback",
        "title": "Tìm hiểu sản phẩm"
      },
      {
        "payload": "chuong trinh khuyen mai",
        "type": "postback",
        "title": "Thông tin khuyến mại"
      }
    ],
    "composer_input_disabled": false,
    "locale": "vi_VN"
  },
  {
    "call_to_actions": [
      {
        "payload": "toi muon hoi",
        "type": "postback",
        "title": "Ask question"
      },
      {
        "payload": "tim hieu san pham",
        "type": "postback",
        "title": "Our product"
      },
      {
        "payload": "chuong trinh khuyen mai",
        "type": "postback",
        "title": "Promotion"
      }
    ],
    "composer_input_disabled": false,
    "locale": "default"
  }
]
```
![Update Menu](https://bytebucket.org/SolutionPortal/production/raw/4b7f538b3199971b20dd5758445c737644fdd571/assets/images/reply.ai/update-persistent-menu.png)

- Click `Save Changes` button
- Your greeting message will be shown as

    ![FB Persistent Menu](https://bytebucket.org/SolutionPortal/production/raw/4fd05874510a3c6a89a14176017198bbde71154b/assets/images/reply.ai/fb-persistent-menu.png)

-  You can show the following list in menu by updating `payload` parameter and `title`.

#### 1. Current Customer

|payload			|title						|
|-------------------|-----------------  		|
|hop dong va phi	|Hợp đồng và phí			|
|mua them hop dong	|Mua thêm hợp đồng			|

#### 2. Prospect

|payload			|titlte						|
|-------------------|-----------------  		|
|toi muon hoi		|Tôi muốn hỏi				|
|mua bao hiem		|Mua bảo hiểm				|
|tu van san pham	|Tư vấn sản phẩm			|

#### 3. Product Exploration

|payload			|titlte						|
|-------------------|-----------------  		|
|tim hieu san pham	|Tìm hiểu sản phẩm			|
|tich luy			|Tích lũy					|
|hoc van cua con	|Học vấn của con			|
|tuoi nghi huu		|Tuổi nghỉ hưu				|
|bao ve				|Bảo vệ						|
|tai nan			|Tai nạn					|
|tu vong thuong tat	|Tử vong, thương tật		|
|benh hiem ngheo	|Bệnh hiểm nghèo			|
|dau tu				|Đầu tư						|
|nhu cau khac		|Nhu cầu khác				|

#### 4. Promotion

|payload				|tilte					|
|-----------------------|---------------------  |
|thong tin khuyen mai	|Thông tin khuyến mại	|
|quay so trung thuong	|Quay số trúng thưởng	|
|qua tang lien tay		|Quà tặng liền tay		|

### 5. Other

|payload			|title						|
|-------------------|-----------------			|
|mini game			|Tham gia mini games		|
|thac mac va lien he|Thắc mắc và liên hệ		|
|tim hieu them		|Tìm hiểu thêm				|

Please noted that Facebook support only 3 options in the menu. You should pick the 3 most visited features.