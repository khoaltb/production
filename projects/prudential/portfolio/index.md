---
layout: page
title: Home
permalink: /
description: Project Overview

slider-image1: /assets/images/slider/home/chatbot_architecture.png
slider-image2: /assets/images/slider/home/project_planning.png
slider-image3: /assets/images/slider/home/feature_list.png
slider-caption1: Chatbot Architecture & Technology (High Level)
slider-caption2: Lead-Generation Chatbot Implementation Plan
slider-caption3: Lead-Generation Chatbot Feature List
---

<br>

# Project Overview

## Client Background

[Prudential Vietnam Company Ltd.](https://www.prudential.com.vn/) (Prudential Viet Nam), a member of Prudential plc. (United Kingdom) was the first fully foreign owned consumer finance company which has pioneered providing customized financial consultancy and loan programs to different customer segments. With the widespread business network nationwide, the company has capability to serve customers with flexible financial solutions, enabling our customers to fulfill their dreams and enhance their living standards. 

## Client Business Need

- Cut down and eliminate phone calls or other customer support that are already using.
- Increase lead generation for new customers. 
- Reduce time spend by support members. 

## Project Scope Summary
A chat-bot – new generation of non-voice messenger automation will be developed to get information of customer.
Those information will be forwarded to an agent (telesales) to follow up.

# Feature List
## Support Current Customer

![](/assets/images/home/current-customer.png)

## Support Prospect

![](/assets/images/home/prospect.png)

### Product Exploration
- Expand the product types by category

![](/assets/images/home/product-category.png)

### Make Appointment
- Make a appointment with telesales

![](/assets/images/home/product-consulting.png)


## Promotion
- Display the promotion 

![](/assets/images/home/promotion.png)

- Encourage users to join Mini games

![](/assets/images/home/mini-game.png)

# Timeline
9 weeks, plus 1 week of Go-Live and production support.
