---
layout: post
title: Checklist
permalink: /checklist/
description: Lead Generation Chatbot from Facebook Messenger, Reply.ai, and AWS NodeJS.
categories: LeadGenerationChatbot
---

## Environment Checklist


### 1. Facebook Page Checklist

{: .table .table-striped}
|  #  | No | Items                                      | Production Data                                                                	|
|:---:|:--:| ------------------------------------------ | ----------------------------------------------------------------------------------|
| [x] | 01 | Facebook Page Name                         | [Bảo hiểm Nhân thọ Prudential Việt Nam](https://www.facebook.com/Prudential.pva/) |
| [x] | 02 | Facebook Page Type                         | Insurance                                                                         |
| [x] | 03 | Facebook Avatar photo                      | assets/images/fb-page/avatar.png                                                  |
| [x] | 04 | Facebook Cover photo                       | assets/images/fb-page/cover.png                                                   |
| [x] | 05 | Facebook Messenger Greeting Message        | Chào [name]! Hãy nhấn vào nút BẮT ĐẦU/GET STARTED để chat với [BotName] nhé!   	|
| xxx | xx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |


### 2. Facebook App Checklist

|  #  | No | Items                                      | Production Data                                                               	|
|:---:|:--:| ------------------------------------------ | ----------------------------------------------------------------------------------|
| [x] | 01 | Facebook account   						|username:  <br/> password:															|
| [x] | 02 | Facebook App Icon  						|assets/images/fb-page/app-icon.png													|
| [x] | 03 | App Long Description  						|This app is our attempt to communicate with customers through an automated answering machine. This app will create a fun and interactive experience to Prudential's customers through many touch points. Many customers ask repetitive questions, which requires a huge amount of staff time dedicated to this task. With this chatbot app, we hope to serve our customers quicker and with a higher standard												|
| [x] | 04 | Privacy Policy URL 						|[https://www.prudential.com.vn/vi/footer/privacy-policy/](https://www.prudential.com.vn/vi/footer/privacy-policy/) 					  	  |
| [x] | 05 | AWS Server URL (Webhook URL)   			|chatbot.prudential.com.vn                                   						|
| [x] | 06 | Facebook `Page Access Token`               |                                   												|
| [x] | 07 | Facebook `App Secret`            			|                                   												|
| xxx | xx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |


### 3. Reply.ai Checklist

|  #  | No | Items                                      | Production Data                                                               	|
|:---:|:--:| ------------------------------------------ | ----------------------------------------------------------------------------------|
| [x] | 01 | Reply account: Prudential Assurance VN     |username: hd.vu@prudential.com.vn <br/> password:									|
| [x] | 02 | Import the flow    						|/assets/reply.ai/build/flows/flows.json											|
| [x] | 03 | Import the content 						|/assets/reply.ai/build/content/contents.json										|
| [x] | 04 | Update `api_base` param 					|																					|
| [x] | 05 | Configure Greeting message     			| 																					|
| [x] | 06 | Configure Persistent Menu      			|																					|
| [x] | 07 | Configure `email_auth.json`				|host:  mailgate.prudential.com.vn <br/> port: 25 <br/> user: chatbot@prudential.com.vn <br/> pass: |
| [x] | 08 | Update `CALL_CENTER_EMAIL` param       	|customer.service@prudential.com.vn													|
| [x] | 09 | Update `CALL_CENTER_EMAIL_CC` param    	|customer.service@prudential.com.vn													|
| [x] | 10 | Update `CALL_CENTER_EMAIL_REPORT` param    |customer.service@prudential.com.vn													|
| [x] | 11 | Update `LEAD_GENERATION_EMAIL` param   	|p.xuanan@prudential.com.vn															|
| [x] | 12 | Update `LEAD_GENERATION_EMAIL_CC` param	|p.xuanan@prudential.com.vn															|
| [x] | 13 | Update `LEAD_GENERATION_EMAIL_REPORT` param|p.xuanan@prudential.com.vn	        												|
| [x] | 14 | Update `MINI_GAME_EMAIL` param 			|na.thi@prudential.com.vn 															|
| [x] | 15 | Update `ON_OFF_PROMOTION` param    		|on 																				|
| [x] | 16 | Update `ON_OFF_PRODUCT` param  			|on																					|
| [x] | 17 | Update `ON_OFF_MINI_GAME` param    		|on																					|
| xxx | xx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |

### 4. AWS Server Checklist

|  #  | No | Items                                      | Production Data                                                               	|
|:---:|:--:| ------------------------------------------ | ----------------------------------------------------------------------------------|
| [x] | 01 | Amazon account 							|username  <br/> password 															|
| [x] | 02 | Upgrade Ubuntu Packages    				|`sudo apt-get update && sudo apt-get upgrade`										|
| [x] | 03 | Install & Configure NodeJS v6.10.3 LTS 	|`mkdir download` <br/> `cd download` <br/> `curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -` <br/> `sudo apt-get install nodejs`			 					| 
| [x] | 04 | Upgrade npm                				|`sudo npm install -g npm@latest`													|
| [x] | 05 | Check version node         				|`node --version`																	|
| [x] | 06 | Check version npm          				|`npm --version`																	|
| [x] | 07 | Import the MongoDB repository key			|`sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6`																	|
| [x] | 08 | Create a list file for MongoDB 			|`echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" |` <br/> `sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list`			|
| [x] | 09 | Update the packages list       			|`sudo apt-get update`																|
| [x] | 10 | Install mongodb-org    					|`sudo apt-get install mongodb-org` <br/> `sudo systemctl start mongod`				|
| [x] | 11 | Restart MongoDB        					|`sudo systemctl enable mongod`														|
| [x] | 12 | Enable UFW             					|`sudo ufw enable`																	|
| [x] | 13 | Allow access to MongoDB 					|`sudo ufw allow 27017` <br/> `netstat -plntu` <br/> `netstat -anp | grep 27017`	|
| [x] | 14 | Configure a Public BindIP 					|`ifconfig` <br/> `sudo nano /etc/mongod.conf` <br/> `sudo systemctl restart mongod`|
| [x] | 15 | Test the Remote Connection 				|`mongo --host ip_address`															|
| [x] | 16 | Install PM2 								|`sudo npm install -g pm2` <br/> `pm2 startup systemd`								|
| [x] | 17 | Configure DNS 								| 																					|
| [x] | 18 | Clone source code from bitbucket 			|`https://bitbucket.org/SolutionPortal/production/projects/prudential/` > `/source/Node.js/`  																					|
| [x] | 19 | Run server with PM2        				|`cd production/projects/prudential/source/Node.js` <br/> `npm install` <br/> `pm2 start index.js`																				|
| [x] | 20 | Install crontab        					|`sudo yum install vixie-cron crontabs` <br/> `sudo /sbin/service crond start`		|
| [x] | 21 | Update write permission `call_center_report_template.json.json`|`sudo chmod 646 /opt/production/projects/prudential/source/Node.js/config/email/call_center_report_template.json`										|
| [x] | 22 | Update write permission `lead_generation_report_template.json`	|`sudo chmod 646 /opt/production/projects/prudential/source/Node.js/config/email/ead_generation_report_template.json` 							|
| [x] | 23 | Check the r/w permission of file			|ls -l /opt/production/projects/prudential/source/Node.js/config/email				|
| [x] | 24 | Update time for LG & CC report				| `sudo crontab -e` <br/> `00 16 * * * curl https://chatbot.prudential.com.vn/reportCallCenter` <br/> `00 16 * * * curl https://chatbot.prudential.com.vn/reportLeadGeneration`	|
| [x] | 25 | Update reporting email to server    		| Send message to bot <br/> `update_email_call_center_report` <br/> `update_email_lead_generation_report` 																		|
| [x] | 26 | Update encrypt key in database   		    | /source/Node.js/config/environment/auth_key.json <br/> Update `database_auth_key` |
| [x] | 27 | Restart Cron job   		    			| sudo /etc/init.d/cron start 														|
| xxx | xx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx |

```
 ssh -i xxx.pem ubuntu@13.59.191.218

 cd /opt/
 sudo git clone  https://bitbucket.org/SolutionPortal/production/projects/prudential/
 // cd /opt/production/projects/prudential/source/Node.js/
 // sudo git pull

 cd /opt/production/projects/prudential/source/Node.js/
 sudo npm install

 pm2 start index.js
 // curl https://chatbot.prudential.com.vn/check

 // pm2 list
 // pm2 status

 sudo nano /opt/production/projects/prudential/source/Node.js/config/email/email_auth.json

 // pm2 stop all
 // pm2 kill
 // pm2 log
 // curl https://chatbot-staging.prudential.com.vn/check

```