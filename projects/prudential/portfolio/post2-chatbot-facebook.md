---
layout: post
title: Facebook
permalink: /facebook/
description: Lead Generation Chatbot from Facebook Messenger, Reply.ai, and AWS NodeJS.
categories: LeadGenerationChatbot
---

## {{ page.title }}

## 1. Setting up Facebook Page 
To create a Facebook bot we need 2 things:

-   **Facebook public page** you’ll connect your bot to.
-   **Facebook Developer application** which will be connected to your webhook server and your public page and work as a middleware between them.

So, at first you need to create a page. Go to [Facebook Page Creation](http://www.facebook.com/pages/create), then select Page Type

![](/assets/images/fb-developer/facebook-page_category.png)

Next, choose a category, subcategory and click `Get Started`.

![](/assets/images/fb-developer/facebook-page-type.png)

### Update avatar and cover photo

![](/assets/images/fb-developer/facebook-page.png)

1. Select the avatar photo
2. Select the cover photo
3. Set up the `Send Message` button for your Facebook page.

### Setting up the "Send Message" button for Messenger
- Click `Add button` button to set up the `Get Started` button for your Messenger
- Select `Contact you`, then `Send Message` option and `Next`

![](/assets/images/fb-developer/facebook-add-button-to-page.png)

Your Buton `Send Message` is set up.

![](/assets/images/fb-developer/facebook-add-get-started-button.png)

### Update Messenger Greeting
- Go to `Settings`
- Click `Messaging`
- Turn on `Greeting Message` as `Yes`

![](/assets/images/fb-developer/facebook-page-settings.png)

- Click `Change` button to update message

![](/assets/images/fb-developer/facebook-page-setting-greeting.png)

![](/assets/images/fb-developer/facebook-messenger-greeting-2.png)

- Add Personlisation and `Save` button to update your Messenger Greeting.

After that you’ll need to create an app. 

## 2. Setting up Facebook Application
- Go to [Facebook Developer](https://developers.facebook.com/apps/), to create your Facebook App 

![](/assets/images/fb-developer/facebook-developer-app.png)

- Click button `Add a New App`

![](/assets/images/fb-developer/facebook-create_app.png)

- Fill `Display Name` and `Contact Email` -> Click button `Create App ID`
- Now your Facebook app is successfully created 

## 3. Integrate chatbot to Facebook page
After creating an app click `Add Product` from the left menu, then choose `Messenger` and click `Set Up` button.

![](/assets/images/fb-developer/facebook-app-messenger.png)

At the `Token Generation` section, choose the page you just created in order to generate a token. Facebook may request your permission before generating token. Don't worry, this process happens only once time, you just follow the instruction.

![](/assets/images/fb-developer/facebook-get-Page%20Access%20Token.png)

#### Setting up Webhook

Click on `Setup Webhooks` button to fill the following information

![](/assets/images/fb-developer/facebook-app-setup-webhooks.png)

1. `Callback URL` from Reply.ai console > `Integrations` > `Webhook URL`
2. `Verify Token` from Reply.ai console > `Integrations` > `Verify Token`
3. Check messages in the `Subcription Fields` section below.
	
![](/assets/images/fb-developer/facebook-app-page-subscription.png)

Click `Verify and Save` button to complete the `Reply.ai Integration`.

For item #1 and #2, you can go to Reply.ai to get `Callback URL` and `Verify Token`
    
![](/assets/images/fb-developer/reply-callback-url.png)

## 4. Test your page
- Go to [https://developers.facebook.com/](https://developers.facebook.com/)
- Select `Roles` menu on the left side.
- Add FB ID of your testers

![](/assets/images/fb-developer/fb-add-tester.png)

Now, your bot is ready to test.

## 5. Submit for App Review
This is the final step to go live. Go to the `Messenger` section and click `Settings`, and then `Basic`.

### Before App Submission
There are a few things you'll want to take care of to make the process as easy as possible. The following fields are required before submitting

- App Icon: your icon should be a JPG, GIF or PNG file. The size of the image must be 1024 x 1024 pixels. File size limit 5 MB.

- Long Description: `This app is our attempt to communicate with customers through an automated answering machine. This app will create a fun and interactive experience to Prudential's customers through many touch points. Many customers ask repetitive questions, which requires a huge amount of staff time dedicated to this task. With this chatbot app, we hope to serve our customers quicker and with a higher standard`

- Privacy Policy: https://www.prudential.com.vn/vi/footer/privacy-policy/

- Category: it should be `App for Messengers`.

![](/assets/images/fb-developer/facebook-app-review.png)

### Submission

- Under `Messenger` menu, look the `App Review for Messenger` dialog. Once there, locate the `pages_messaging` permission; this is the most important permission and, for simple bots, the only one you'll need. Click on `Add to Submission`:

![](/assets/images/fb-developer/add-to-submission.png)

- Find the `Current Submission` dialog, Click on the `Edit Notes` link to the right of `pages_messaging`

![](/assets/images/fb-developer/edit-pages-messaging.png)

- Fill some notes for the Facebook reviewer.

![](/assets/images/fb-developer/api-pages-messaging.png)

	1. Select a Page
	2. Write an example of user message with its bot answer.
	3. Click `Save`

Sample notes for Prudential Chatbot

| No | Command                                  | Automated Reply                                                                							|
|:--:| ----------------------------------------	| --------------------------------------------------------------------------------							|
| 01 | Prubot									| Chào bạn @fullname. Rất vui được gặp lại bạn. <br /> Bạn @fullname đã có hợp đồng với Prudential chưa nè?	|
| 02 | thac mac va lien he                     	| PruBot có thể giúp bạn																					|
| 03 | tim hieu them                   		  	| Bạn có thể tìm hiểu thêm thông tin tại đây. https://www.donghanhchocuocsongtotdep.vn/quatangdonghanh		|
| 04 | giai phap tich luy                   	| Bạn đang muốn tích lũy cho kế hoạch nào sau đây:															|
| 05 | giai phap bao ve	                  		| Bạn đang muốn dự phòng trước																				|

- Update `Optional Notes for Reviewer`: `@fullname is your Facebook full name`	

You have now submitted your app to Facebook for review. Keep an eye on your Facebook notifications, as soon you'll see activity from Facebook reviewers and a Developer Alert

![](/assets/images/fb-developer/messenger-submission.png)


If everything went right, you'll see that the `pages_messaging` permission is marked with a green check, and that the **Results From Last Submission** are `Approved`

![](/assets/images/fb-developer/app-review-approval.png)


Now your bot is live and visible to everyone! The last step is making sure that the app is indeed public.
- Go to App Review and check that the switch at the top is set to Yes

![](/assets/images/fb-developer/make-page-public.png)

For more information, refer [How to Submit for Feature Review](https://developers.facebook.com/docs/facebook-login/review/how-to-submit)

- Make your page is public, then `Start a Submission`