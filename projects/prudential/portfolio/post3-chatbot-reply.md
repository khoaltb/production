---
layout: post
title: Reply.ai
permalink: /reply.ai/
description: Lead Generation Chatbot from Facebook Messenger, Reply.ai, and AWS NodeJS.
categories: LeadGenerationChatbot
---

## {{ page.title }}

## I. Activate the Reply account
- You will receive an invitation from Reply.ai to activate your account.

    ![](/assets/images/reply.ai/invitation-email.png)

- Click on the link to activate your account.

## II. Deploy the flows into Reply
1. Log into Reply.ai
2. Select `Flows` menu on the left side.
3. Open the gear icon on the right corner and click `Import`

    ![Import Flow Menu](/assets/images/reply.ai/import-flow-menu.png)

4. Select **Choose File -> Browse to flow Json file -> Import**
The import file template is available at [production/projects/prudential/assets/reply.ai/build/flows](production/projects/prudential/assets/reply.ai/build/flows)

    ![Import flows](/assets/images/reply.ai/reply-import-flow.png)

> Now, the flow is successfully imported.
    ![Import Flow](/assets/images/reply.ai/import-flow-successfully.png)

## III. Deploy the content into Reply
1. Select `Content` menu on the left side.
2. Open the gear icon on the right corner and click `Import`

    ![Import Content Menu](/assets/images/reply.ai/import-content-menu.png)
3. Select **Choose File -> Browse to content Json file -> Import**
The import file template is available at [production/projects/prudential/assets/reply.ai/build/content](production/projects/prudential/assets/reply.ai/build/content)

    ![Import Content](/assets/images/reply.ai/reply-import-content.png)

> Now, the content is successfully imported.
    ![Import Content](/assets/images/reply.ai/import-content-successfully.png)

## IV. Connect AWS server to Reply.ai
1. Select `Content` menu on the left side.
2. Select `api_base` parameter and click `Edit` icon

    ![Select API_BASE parameter](/assets/images/reply.ai/configure-api.png)

3. Fulfill the value by your `AWS Server URL (Webhook URL)` and click `Save Changes` button

    ![API_BASE](/assets/images/reply.ai/api-base-update-content.png)


## V. Integrate with Facebook Page

1. Select `Integration` menu on the left side.

    ![Integration](/assets/images/reply.ai/reply-connect-facebook.png)

2. Click `Connect` Facebook button

![Integration](/assets/images/reply.ai/reply-connect-facebook-2.png)

`Page Access Token` and `App secret` from  `https://developers.facebook.com` can be get from [Facebook Developer page](https://developers.facebook.com/apps/)
    - Page Acess Token

![](/assets/images/fb-developer/facebook-get-Page%20Access%20Token.png)

    - App Secret

![](/assets/images/fb-developer/facebook-app-dashboard.png)

3. Click `Submit` to complete the Facebook Integration flow.

## VI. Configure Greeting Message & Persistent Menu
1. Go to `Integration` section, then select `Facebook` and click `Settings` button

    ![](/assets/images/reply.ai/reply-integration.png)
	
2. Go to `Messenger configuration` section and click `Bot Configuration` button

    ![](/assets/images/reply.ai/bot-configuration.png)

### Configure `Get Started` button

- Select `Get Started Button` option

![Get Started](/assets/images/reply.ai/reply-config-get-started.png)

> Enter `menu` and click `Save Changes` button

### Update Greeting text

- Select `Greeting text` option
- Paste the following text

```
[
  {
    "text": "Chào {{user_full_name}}! Bạn có thắc mắc và cần bạn đồng hành giải đáp? Hãy nhấn vào nút BẮT ĐẦU/GET STARTED để chat với Prudential nhé!",
    "locale": "default"
  }
]
```

![Update Greeting Text](/assets/images/reply.ai/update-greeting-text.png)

- And click `Save Changes` button
- Your greeting message will be shown as

    ![Greeting Message](/assets/images/reply.ai/fb-greeting-message.png)

### Update Persistent Menu

- Select `Persistent Menu` option
- Paste the following text

```
[
  {
    "call_to_actions": [
      {
        "payload": "toi muon hoi",
        "type": "postback",
        "title": "Tôi muốn hỏi"
      },
      {
        "payload": "tim hieu san pham",
        "type": "postback",
        "title": "Tìm hiểu sản phẩm"
      },
      {
        "payload": "chuong trinh khuyen mai",
        "type": "postback",
        "title": "Thông tin khuyến mại"
      }
    ],
    "composer_input_disabled": false,
    "locale": "vi_VN"
  },
  {
    "call_to_actions": [
      {
        "payload": "toi muon hoi",
        "type": "postback",
        "title": "Ask question"
      },
      {
        "payload": "tim hieu san pham",
        "type": "postback",
        "title": "Our product"
      },
      {
        "payload": "chuong trinh khuyen mai",
        "type": "postback",
        "title": "Promotion"
      }
    ],
    "composer_input_disabled": false,
    "locale": "default"
  }
]
```

![Update Menu](/assets/images/reply.ai/update-persistent-menu.png)

- Click `Save Changes` button
- Your greeting message will be shown as

    ![FB Persistent Menu](/assets/images/reply.ai/fb-persistent-menu.png)

-  You can show the following list in menu by updating `payload` parameter and `title`.

#### 1. Current Customer

|payload				|title					|
|-----------------------|-----------------------|
|hop dong va phi		|Hợp đồng và phí		|
|mua them hop dong		|Mua thêm hợp đồng		|
| xxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxx |

#### 2. Prospect

|payload				|titlte					|
|-----------------------|-----------------------|
|toi muon hoi			|Tôi muốn hỏi			|
|mua bao hiem			|Mua bảo hiểm			|
|tu van san pham		|Tư vấn sản phẩm		|
| xxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxx |

#### 3. Product Exploration

|payload				|titlte					|
|-----------------------|-----------------------|
|tim hieu san pham		|Tìm hiểu sản phẩm		|
|tich luy				|Tích lũy				|
|hoc van cua con		|Học vấn của con		|
|tuoi nghi huu			|Tuổi nghỉ hưu			|
|bao ve					|Bảo vệ					|
|tai nan				|Tai nạn				|
|tu vong thuong tat		|Tử vong, thương tật	|
|benh hiem ngheo		|Bệnh hiểm nghèo		|
|dau tu					|Đầu tư					|
|nhu cau khac			|Nhu cầu khác			|
| xxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxx |

#### 4. Promotion

|payload				|tilte					|
|-----------------------|-----------------------|
|thong tin khuyen mai	|Thông tin khuyến mại	|
|quay so trung thuong	|Quay số trúng thưởng	|
|qua tang lien tay		|Quà tặng liền tay		|
| xxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxx |

### 5. Other

|payload				|title					|
|-----------------------|-----------------------|
|mini game				|Tham gia mini games	|
|thac mac va lien he	|Thắc mắc và liên hệ	|
|tim hieu them			|Tìm hiểu thêm			|
| xxxxxxxxxxxxxxxxxxxxx | xxxxxxxxxxxxxxxxxxxxx |

Please noted that Facebook support only 3 options in the menu. You should pick the 3 most visited features.

## VII. Integrate Reply.ai with Corporate Website

1. Go to `Integration` section, then select `Website` and click `Add` button

![web Integration](	/assets/images/reply.ai/web-integration.png)

2. Enter your Widget name

![Widget](/assets/images/reply.ai/web-integration-widget-name.png)

3. Include the widget

![Update Menu](/assets/images/reply.ai/web-integration-snippet.png)

Copy and paste this snippet at the end of the closing tag of the body element of your client page.

```    
    <script type="text/javascript">
		var widgetSettings = {
		channelUuid: "xxxx-xxxx-xxxx-xxxx-xxxx",
		style: "fixed", 
		fixedCloseMode: "fixed",
		welcomeMessage : {
			imageUrl: "https://giphy.com/gifs/animation-cute-smile-3o7TKMt1VVNkHV2PaE",
			text : "Chào bạn, mình là #BotName ✌ - robot hỗ trợ trực tuyến 24/7 của bạn đây!",
		},
		customText: {
			headerText: "PruBot",
			closedHeaderText: "Chat with PruBot",
			botName: "PruBot",
		},
		customStyles: {
			headerBgColor: "#ED1B2E",
			borderColor: "#ED1B2E"
		};
		window.replySDK = (function(d, s, id) {
			var o = window.replySDK || {};var js,
			fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src ="https://d1fidecqhnmd5.cloudfront.net/prod/v1.3.9/dist/js/reply-sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
			o.isLoaded=false;o._e = [];o.load = function(f) {
				o._e.push(f);
			};
			js.onload = function () { 
				replySDK.init( widgetSettings ); 
			};
			return o;
		}
		(document, 'script', 'reply-sdk-js'));
	</script>

```

|Property                   |Default value  |Description                                   											|
|---------------------------|---------------|---------------------------------------------------------------------------------------|
|channelUuid    			|(required)	    |Web Widget Channel UUID, find it in the configuration of your channel					|
|style	        			|"fixed"		|"fixed": <br /> "embed": <br /> "fullpage":    										|
|fixedCloseMode 			|"normal"       |(Only for style:fixed) "normal" or "small". Used in fixed style when widget is closed	|
|customStyles.headerBgColor	|"#ED1B2E"	    |Header background color   																|
|customStyles.borderColor	|"#ED1B2E"	    |Border color   																		|