## Install lastest modules (Node version > 3.11):
* Install package npm-check-updates: `npm install -g npm-check-updates`
* Check for updates: `ncu`
* Update package.json: `ncu -a`
* Install packages: `npm install`

## RUN
- node index.js

## TEST
- Open browser
- Go to: http://localhost:8080/
- Should return: Server runs successfully!
