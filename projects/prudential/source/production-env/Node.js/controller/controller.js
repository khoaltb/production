/**
* main_controller.js:
* - holds functions regarding to calculation
* - ...
*/
const EMAIL_CONFIG_PATH = global.rootPath + '/config/email/';
const TEMPLATE_CONFIG_PATH = global.rootPath + '/config/template/';
var logger = require(global.rootPath + '/controller/logger.js');
var factory = require(global.rootPath + '/controller/utils/object_factory.js');
var utils = require(global.rootPath + '/controller/utils/utils.js');
var constants = require(global.rootPath + '/controller/utils/constants.js');
var moment = require('moment');

exports.sendEmail = function(contentObject, callback) {
  var json = {
          "status": false
        };

  if (!contentObject || !contentObject.email.to) {
    return callback(json);
  }

  console.log("Start send email");

  // Initialize trasporter to send email
  var transporter = factory.createTransporter();
  var filePath = EMAIL_CONFIG_PATH + contentObject.email.type + ".json";

  utils.readFile(filePath , function (err1, data) {
    // Read file ics then send email
    var mailObj = factory.createMailObject(data.format, contentObject);
    logger.debug("Content from request: " + JSON.stringify(contentObject));

    // Send mail
    transporter.sendMail(mailObj, function(err2, info){
      // Return success result
      if(err2 == null){
        json.status = true;
        logger.debug("Info "+ JSON.stringify(info));
      } else {
        logger.debug("Error "+ err2);
      }

      // Return result
      callback(json);
    });
  });
};

const EXCEL_EXTENTION = '.xlsx';
exports.sendExcelToEmail = function(type, excelFile, callback){
  var templatePath = EMAIL_CONFIG_PATH + type + ".json";
  var transporter = factory.createTransporter();
  var result = {
          "status": false
        };

  // logger.debug("Step 1");
  utils.readFile(templatePath , function (err1, data) {
    // logger.debug("Step 2");

    var today = moment().format("DD/MM/YYYY");
    var json = data.format;
    json.subject = json.subject.replace("dd/mm/yyyy", today);
    json.html = json.html.replace("dd/mm/yyyy", today);
    json.attachments[0].filename = excelFile.name + EXCEL_EXTENTION;
    json.attachments[0].content = new Buffer(excelFile.data,'uft-8');

    // logger.debug("Step 3");
    transporter.sendMail(json, (err2, info) => {
      // Return success result
      if(err2 == null){
        result.status = true
        logger.debug("Info "+ JSON.stringify(info));
      } else {
        logger.debug("Error "+ err2);
      }
      // logger.debug("Step 4");
      // Return result
      return callback(result);
    });
  });
}

exports.updateEmailReport = function(req, callback) {
  // var SelfReloadJSON = require('self-reload-json');
  // var file = new SelfReloadJSON(global.rootPath + '/config/email/lead_generation_report_template.json');
  var fs = require('fs');
  var fileName = global.rootPath + '/config/email/lead_generation_report_template.json';
  if (req.body.type === 'call_center') {
    fileName = global.rootPath + '/config/email/call_center_report_template.json';
  }
  var file = require(fileName);

  // Update from email and to email
  if (req.body.from) {
      file.format.from = req.body.from;
  }

  if (req.body.to) {
    file.format.to = req.body.to;
  }

  logger.debug(JSON.stringify(file));
  var result = {
    "status": true
  }
  fs.writeFile(fileName, JSON.stringify(file, null, 2), function (err) {
    if (err) result.status = false;
    return callback(result);
  });
}

exports.getNext5Days = function(channel, question, callback) {
  var result = {
    "status": false,
    "dates": ""
  };

  var next5Days = getNext5Days();
  console.log(next5Days.length);
  var filePath = TEMPLATE_CONFIG_PATH + "facebook_template.json";
  if (channel === constants.WEB_CHANNEL) {
    filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
  }
  utils.readFile(filePath , function (err, source) {
      if (err) {
          return callback(result);
      }

      var buttons = [];
      for (var i = 0; i < next5Days.length; i++) {
        var button = utils.createButton("quick_reply", channel);
        button.title = next5Days[i];
        button.payload = next5Days[i];
        buttons.push(button);
      }

      if (buttons.length > 0) {
        if (channel === constants.WEB_CHANNEL) {
          template = source.website_replies;
          template.text = question;
          template.quick_reply = buttons;
        } else {
          template = source.facebook_replies;
          template.text = question;
          template.quick_replies = buttons;
        }

        result.dates = template;
        result.status = true;
      }
      console.log(JSON.stringify(result));
      callback(result);
    });
}

// exports.getNext5Days = function(question, callback) {
//   var result = {
//     "status": false,
//     "dates": ""
//   };
//
//   var next5Days = getNext5Days();
//   console.log(next5Days.length);
//   var filePath = TEMPLATE_CONFIG_PATH + "facebook_template.json";
//   utils.readFile(filePath , function (err, source) {
//       if (err) {
//           return callback(result);
//       }
//
//       var buttons = [];
//       for (var i = 0; i < next5Days.length; i++) {
//         var button = utils.createButton("quick_reply");
//         button.title = next5Days[i];
//         button.payload = next5Days[i];
//         buttons.push(button);
//       }
//
//       if (buttons.length > 0) {
//         var template = source.facebook_replies;
//         template.text = question;
//         template.quick_replies = buttons;
//
//         result.dates = template;
//         result.status = true;
//       }
//       console.log(JSON.stringify(result));
//       callback(result);
//     });
// }

var getNext5Days = function(today) {
  // Get next 5 days from today
  var max = 5;
  var daysInWeek = ["Chủ Nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"];
  var contactDates = [];
  for (var i = 1; i <= max; i++) {
    // Get date
    var date = moment(today).add(i, 'days');

    var dateWithFormat = moment(date).format("DD/MM/YYYY");
    // console.log("Date with Format: " + dateWithFormat);

    var dateNoYear = dateWithFormat.substring(0, dateWithFormat.lastIndexOf("/"));
    // console.log("Date no year: " + dateNoYear);

    var index = date.day();
    var day = daysInWeek[index];
    if (i == 1) {
      day = "Ngày mai";
    } else if (i == 2) {
      day = "Ngày mốt";
    }

    date = day + ", " + dateNoYear;
    contactDates.push(date);
  }

  return contactDates;
}
