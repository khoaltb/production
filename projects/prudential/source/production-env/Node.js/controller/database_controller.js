var mongoose = require('mongoose');
var logger = require(global.rootPath + '/controller/logger.js');
var dbUtils = require(global.rootPath + '/models/db_util.js');
var utils =  require(global.rootPath + '/controller/utils/utils.js');

var Contract = require(global.rootPath + '/models/contract.model');
var Product = require(global.rootPath + '/models/product.model');
var User = require(global.rootPath + '/models/user.model');
var ProductInterest = require(global.rootPath + '/models/product_interest.model');
var Promotion = require(global.rootPath + '/models/promotion.model');
var PromotionInterest = require(global.rootPath + '/models/promotion_interest.model');
var Question = require(global.rootPath + '/models/question.model');

exports.saveProductInterest = function(req, callback) {
  var result = {
      "status": false
    };

  var user = req.body.user;
  var userData = dbUtils.createUserData(user);
  insert("User", userData, function(output1){
    if (!output1.status) {
      return callback(result)
    }

    var userId = output1._id;
    var data = dbUtils.createProductInterestData(userId, req.body);
    logger.debug("Data: " + output1._id + JSON.stringify(data));
    insert("ProductInterest", data, function(output2){
      if (!output2.status) {
        return callback(result)
      }

      result.status = true;
      return callback(result);
    });
  });
}

exports.clearData = function(json){
  var model = mongoose.model(json.collection_name);
  var result = {
    "status": true
  }
  model.remove(json.condition, function(err){
      if (err) {
        result.status = false;
      }

      return callback(result);
  });
}

exports.saveQuestion = function(req, callback) {
  var result = {
      "status": false
    };

  var user = req.body.user;
  var userData = dbUtils.createUserData(user);
  insert("User", userData, function(output1){
    if (!output1.status) {
      return callback(result)
    }

    var userId = output1._id;
    var data = dbUtils.createQuestionData(userId, req.body.question);
    logger.debug("Data: " + output1._id + JSON.stringify(data));
    insert("Question", data, function(output2){
      if (!output2.status) {
        return callback(result)
      }

      result.status = true;
      return callback(result);
    });
  });
}

var insert = function(modelName, data, callback){
    // logger.debug("Step 1" + data);
    var schema = mongoose.model(modelName).schema;
    var model = new (mongoose.model(modelName, schema));
    var result = {
      "status": true,
      "_id": ''
    };

    // logger.debug("Step 2");
    for(fieldName in data){
      model[fieldName] = data[fieldName];
    }

    // logger.debug("Step 3");
    model.save(function(err, output) {
        // logger.debug("Step 4");
        if (err) {
          result.status = false;
          return callback(result);
        };

        if (output._id) {
          result._id = output._id;
        }
        callback(result);
    });

}

var findOneAndUpdate = function(modelName, query, data, callback){
  var model = mongoose.model(modelName);
  var result = {
    "status": true,
    "_id": ''
  }

  model.findOneAndUpdate(query, data , {upsert:true, new:true}, function(err , docs) {
      if (err) {
        result.status = false;
        return callback(result);
      }

      if (docs) {
        result._id = docs._id;
        logger.debug("Docs: " + docs);
        return callback(result);
      } else {
        model.findOne(query, function(err,obj) {
          result._id = obj._id;
          logger.debug("Obj: " + obj);
          return callback(result);
        });
      }
  });
}
