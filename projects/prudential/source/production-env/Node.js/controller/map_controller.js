
exports.getCity = function(req, callback){
  var geocoder = require('geocoder');
  var city;
  let json = {
      "status" : false
  };
  if(req.body.address){
    geocoder.geocode(req.body.address, function ( err, data ) {
        if(err) {
          json.status = false ;
        }else {
          if(data.results[0] && data.results[0].address_components){
              var address_components = data.results[0].address_components;
              for (var i = 0 ; i < address_components.length ; i++) {
                if(address_components[i].types[0].indexOf("administrative_area_level_1") != -1){
                    city = address_components[i].long_name;
                    json.status = true;
                    json.city = req.body.address;
                    json.found = city;
                    break ;
                }
              }

          }else {
            json.status = false ;
          }
        }
        callback(json);
    }, { sensor: true, language : "vi"});
  }else if(req.body.lat_long){
    var location = req.body.lat_long;

    if (!location)  {
        callback(json);
    }

    var latLong = location.split(",");
    if (latLong.length != 2) {
        callback(json);
    }

    geocoder.reverseGeocode(latLong[0], latLong[1], function ( err, data ) {
        let json = {};
        if(err) {
          json.status = false ;
        }else {
          if(data.results[0] && data.results[0].address_components){
              var address_components = data.results[0].address_components;
              for (var i = 0 ; i < address_components.length ; i++) {
                if(address_components[i].types[0].indexOf("administrative_area_level_1") != -1){
                    city = address_components[i].long_name;
                    json.status = true ;
                    json.city = city;
                    break ;
                }
              }
          }else {
            json.status = false ;
          }
        }
        callback(json);
    }, { sensor: true, language : "vi"});
  }else {
    callback(json);
  } 
}