// const TEMPLATE_CONFIG_PATH = global.rootPath + '/config/template/';
var SelfReloadJSON = require('self-reload-json');
var needData = new SelfReloadJSON(global.rootPath + '/config/data/needs_data.json');
var productData = new SelfReloadJSON(global.rootPath + '/config/data/product_data.json');
var logger = require(global.rootPath + '/controller/logger.js');
var utils = require(global.rootPath + '/controller/utils/utils.js');
var constants = require(global.rootPath + '/controller/utils/constants.js');

exports.getNeeds = function(channel, callback){
    var result = {
            "status": false,
            "need": ""
        };

    var filePath = constants.TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }

    utils.readFile(filePath , function (err, source) {
        if (err) {
            return callback(result);
        }

        // Add button
        logger.debug("Template: " + JSON.stringify(needData) + needData.need.length);
        var buttons = [];
        for (var i = 0; i < needData.need.length; i++) {

            var button = createButton("postback");
            // Add title and payload for button
            button.title = needData.need[i].name;
            button.payload = needData.need[i].payload;

            buttons.push(button);

            if (i == 2) {
                break;
            }
        }
        logger.debug("Step 2: Channel " + channel);
        var template;
        if (channel === constants.WEB_CHANNEL) {
            template = source.website_button;
            template.text = needData.sentence;
            template.buttons = buttons;
        } else {
            template = source.facebook_button;
            template.payload.text = needData.sentence;
            template.payload.buttons = buttons;
        }

        logger.debug("Step 5");
        if (buttons.length > 0) {
            result.status = true;
        }

        result.need = template;

        console.log("i: " + JSON.stringify(result));
        return callback (result);
    });
}


exports.getDataById = function(channel, needId, typeId, callback) {
    var filePath = constants.TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }

    var result = {
            "status": false
        };

    if (!needId) {
        return callback(result);
    }

    // Extract number from needId
    // needId = needId.replace(global.AUTH_KEY + " ", "");
    utils.readFile(filePath , function (err, source) {
        // Return error when read file failed or needId is invalid
        if (err || needId < 0 ||
            needId >= needData.need.length) {
            return callback(result);
        }

        var foundData = needData.need[needId];
        if (!foundData) {
            return callback(result);
        }

        var length = foundData.type.length;
        var template = source.facebook_button;
        if (channel === constants.WEB_CHANNEL) {
          template = source.website_button;
        }
        // Show carousel for products
        if ((typeId !== "") || (foundData.sentence === "")) {

            template = source.facebook_carousel;
            if (channel === constants.WEB_CHANNEL) {
              template = source.website_carousel;
            }

            // Find product list
            var productIdList = getProductIdList(typeId, foundData);
            console.log("Product Length: " + productIdList.length)
            var elements = fillElements(channel, needId, typeId, productIdList);

            console.log("Elements: " + JSON.stringify(elements));
            if (channel === constants.WEB_CHANNEL) {
                template.elements = elements;
            } else {
                template.payload.elements = elements;
            }

            if (elements.length > 0) {
                result.status = true;

                if (typeId !== "") {
                    result["product"] = template;
                } else {
                    result["category"] = template;
                }
            }
        }
        // Show buttons for category
        else {
            var buttons = fillButtons(foundData.type);
            console.log("Buttons: " + JSON.stringify(buttons));
            if (channel === constants.WEB_CHANNEL) {
              template.text = foundData.sentence;
              template.buttons = buttons;
            } else {
              template.payload.text = foundData.sentence;
              template.payload.buttons = buttons;
            }

            if (buttons.length > 0) {
                result.status = true;
                result["category"] = template;
            }
        }

        return callback(result);
    });
}

exports.getProductsByTypeId = function(channel, typeId, callback) {
    var filePath = constants.TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }
    var result = {
            "status": false
        };

    if (!typeId) {
        return callback(result);
    }

    // Extract number from needId
    // needId = needId.replace(global.AUTH_KEY + " ", "");
    utils.readFile(filePath , function (err, source) {
        // Return error when read file failed or needId is invalid
        if (err) {
            return callback(result);
        }

        // Find product id list
        var productIdList = [];
        var needId = "";
        for (var i = 0; i < needData.need.length; i++) {
            var tmp = needData.need[i];
            for (var j = 0; j < tmp.type.length; j++) {
                if (typeId === tmp.type[j].id) {
                    needId = tmp.id;
                    productIdList = tmp.type[j].product;
                }
            }
        }

        if (productIdList.length === 0) {
            return callback(result);
        }

        console.log("Product Length: " + productIdList.length)
        var elements = fillElements(channel, needId, typeId, productIdList);

        var template;
        if (channel === constants.WEB_CHANNEL) {
            template = source.website_carousel;
            template.elements = elements;
        } else {
            template = source.facebook_carousel;
            console.log("Elements: " + JSON.stringify(elements));
            template.payload.elements = elements;
        }

        if (elements.length > 0) {
            result.status = true;
            result["product"] = template;
        }

        return callback(result);
    });
}


exports.getOthersByProductId = function(channel, inputId, callback) {
    var result = {
            "status": false,
            "product": {}
        };

    var arrayId = splitInputId(inputId);
    if (arrayId.length != 3) {
        return callback(result);
    }

    var foundData = needData.need[arrayId[0]];
    var tempIdList = getProductIdList(arrayId[1], foundData);    // Temporary product list

    if (tempIdList.length <= 0) {
        return callback(result);
    }

    var k = 0;
    var productIdList = [];
    for (var i = 0; i < tempIdList.length; i++){
        if (arrayId[2] !== tempIdList[i]) {
            productIdList[k] = tempIdList[i];
            k++;
        }
    }

    var filePath = constants.TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }
    utils.readFile(filePath , function (err, source) {
        if (err){
            return callback(result);
        }

        var elements = fillElements(channel, arrayId[0], arrayId[1], productIdList);

        console.log("Elements: " + JSON.stringify(elements));
        var template;
        if (channel === constants.WEB_CHANNEL) {
          template = source.website_carousel;
          template.elements = elements;
        } else {
          template = source.facebook_carousel;
          template.payload.elements = elements;
        }

        if (elements.length > 0) {
            result.status = true;
            result.product = template;
        }

        return callback(result);
    });

}


exports.getProductNameById = function(inputId, callback) {
    var result = {
            "status": false,
            "product_name": ""
        };

    var arrayId = splitInputId(inputId);
    if (arrayId.length != 3) {
        return callback(result);
    }

    var productId = arrayId[2];
    if (!productId || productId < 0 ||
        productId >= productData.product.length) {
        return callback(result);
    }

    result.product_name = productData.product[productId].title;
    result.status = true;

    return callback(result);
}

exports.getProductDetailsById = function(inputId, callback) {
    var result = {
            "status": false,
            "title": "",
            "details": "",
            "details_extra": "",
            "other_product": true
        };

    var arrayId = splitInputId(inputId);
    logger.debug("Input Id: " + inputId);
    if (arrayId.length != 3) {
        return callback(result);
    }

    var productId = arrayId[2];
    if (!productId || productId < 0 ||
        productId >= productData.product.length) {
        return callback(result);
    }

    // Extract number from needId
    logger.debug("Product Id: " + productId);
    var foundProduct = productData.product[productId]
    if (foundProduct.hasOwnProperty('details')){
        result.title = foundProduct.title;
        result.status = true;
        result.details = foundProduct.details[0];
        if (result.details.length >= 2) {
            result.details_extra = foundProduct.details[1];
        }
    }

    // Check if button "Sản phẩm khác" exist
    var foundData = needData.need[arrayId[0]];
    var productIdList = getProductIdList(arrayId[1], foundData);
    if (productIdList.length <= 1) {
        result.other_product = false;
    }

    return callback(result);
}

var splitInputId = function(inputId){
    var arrayId = [];

    if (inputId) {
        arrayId = inputId.split(",");
    }

    console.log("ArrayId Length: " + arrayId.length);
    return arrayId;
}

var getProductIdList = function(typeId, foundData) {
    var productIdList = [];
    for (var i = 0; i < foundData.type.length; i++) {
        if (typeId === foundData.type[i].id) {
            productIdList = foundData.type[i].product;
        }
    }

    return productIdList;
}

var fillButtons = function(category) {
    var length = category.length;
    var buttons = [];

    // Avoid user's redundant data
    if (length >= 3) {
        length = 3;
    }

    for (var i = 0; i < length; i++) {
        var button = createButton("postback");

        // Add title and payload for button
        button.title = category[i].name;
        button.payload = category[i].id;

        buttons.push(button);
    }

    return buttons;
}

var fillElements = function(channel, needId, typeId, productIdList) {
    var elements = [];

    // Create product list elements
    for (var k = 0; k < productIdList.length; k++) {
        var id = productIdList[k];
        var product = productData.product[id];
        var element = createElement();

        // Update info for element
        element.title = product.title;
        element.subtitle = product.subtitle;
        element.image_url = product.image_url;

        if (channel === constants.WEB_CHANNEL) {
          element.image_url = product.image_url_web;

          var defaultAction = {
            "type": "web_url",
            "url": product.item_url
          }
          element.default_action = defaultAction;
        } else {
          element.item_url = product.item_url;
        }

        element.buttons = createProductButtons(needId, typeId, product.id);
        elements.push(element);
    }

    return elements;
}

var createButton = function(type){
    var button = {};
    if (type === "postback") {
        button = {
          "type": "postback",
          "title": "",
          "payload": ""
        }
    }

    return button;
}

var createElement = function(){
    var element = {
            "title":"",
            "image_url":"",
            "subtitle":"",
            "buttons":[]
          }

    return element;
}

var createProductButtons = function(needId, typeId, productId){
    return buttons = [
            {
              "type": "postback",
              "title": "Chi tiết sản phẩm",
              "payload": "chi tiet san pham " + needId + "," + typeId + "," + productId
            }]
}
