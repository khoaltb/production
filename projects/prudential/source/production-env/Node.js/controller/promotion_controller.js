const TEMPLATE_CONFIG_PATH = global.rootPath + '/config/template/';
var SelfReloadJSON = require('self-reload-json');
var promotionData = new SelfReloadJSON(global.rootPath + '/config/data/promotion_data.json');
// var promotionDataTemp = require(global.rootPath + '/config/data/promotion_data_temp.json');
var logger = require(global.rootPath + '/controller/logger.js');
var utils = require(global.rootPath + '/controller/utils/utils.js');
var constants = require(global.rootPath + '/controller/utils/constants.js');

// Get promotion data in Facebook Template
exports.getPromotions = function(channel, callback){
    var result = {
            "status": false,
            "promotion": ""
        };

    var filePath = TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = TEMPLATE_CONFIG_PATH + "website_template.json";
    }

    utils.readFile(filePath , function (err, source) {
        if (err) {
            return callback(result);
        }

        var element = createElement();
        var template;

        element.title = promotionData.title;
        element.subtitle = promotionData.subtitle;
        element.item_url = promotionData.item_url;

        if (channel === constants.WEB_CHANNEL) {
          element.image_url = promotionData.image_url_web;
          template = source.website_carousel;
        } else {
          element.image_url = promotionData.image_url;
          template = source.facebook_carousel;
        }

        // Add button
        var buttons = [];
        for (var i = 0; i < promotionData.promotion.length; i++) {
            var promotion = promotionData.promotion[i];
            var button = createButton("postback");

            button.title = promotion.title;
            button.payload = "chuong_trinh_khuyen_mai" + " " + promotion.id;

            buttons.push(button);
        }

        var elements = [];
        if (buttons.length > 0) {
            element.buttons = buttons;
            elements.push(element);

            if (channel === constants.WEB_CHANNEL) {
              template.elements = elements;
            } else {
              template.payload.elements = elements;
            }

            result.status = true;
            result.promotion = template;
        }

        console.log("i: " + JSON.stringify(result));
        return callback(result);
    });
}

// var createPromotionWeb = function(callback){

//     var filePath = TEMPLATE_CONFIG_PATH + "website_template.json";
//     var result = {
//             "status": false,
//             "promotion": ""
//         };
//
//     utils.readFile(filePath , function (err, source) {
//         if (err) {
//             return callback(result);
//         }
//
//         var template = source.website_carousel;
//         var element = createElement();
//
//         element.title = promotionData.title;
//         element.subtitle = promotionData.subtitle;
//         element.item_url = promotionData.item_url;
//         element.image_url = promotionData.image_url_web;
//
//         // Add button
//         var buttons = [];
//         for (var i = 0; i < promotionData.promotion.length; i++) {
//             var promotion = promotionData.promotion[i];
//             var button = createButton("postback");
//
//             button.title = promotion.title;
//             button.payload = "chuong_trinh_khuyen_mai" + " " + promotion.id;
//
//             buttons.push(button);
//         }
//
//         var elements = [];
//         if (buttons.length > 0) {
//             element.buttons = buttons;
//             elements.push(element);
//             template.elements = elements;
//
//             result.status = true;
//             result.promotion = template;
//         }
//
//         console.log("i: " + JSON.stringify(result));
//         return callback(result);
//     });
// }

exports.getDataByPromotionId = function(channel, promotionId, type, callback){
    var filePath = constants.TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }

    var result = {
            "status": false
        };

    utils.readFile(filePath , function (err, source) {

        if (promotionId < 0 ||
                promotionId >= promotionData.promotion.length) {
                return callback(result);
            }

        var promotion = promotionData.promotion[promotionId];
        if (!promotion) {
            return callback(result);
        }

        var template;
        if (channel === constants.WEB_CHANNEL) {
            template = source.website_button;
            if (type == "time") {
                template.text = promotion.time;
                template.buttons = createTimeButtons(promotionId);

                result["time"] = template;
                result.status = true;
            } else if (type == "condition") {
                template.text = promotion.condition;
                template.buttons = createConditionButtons(promotionId);

                result["condition"] = template;
                result.status = true;
            }
        } else {
          template = source.facebook_button;
          if (type == "time") {
              template.payload.text = promotion.time;
              template.payload.buttons = createTimeButtons(promotionId);

              result["time"] = template;
              result.status = true;
          } else if (type == "condition") {
              template.payload.text = promotion.condition;
              template.payload.buttons = createConditionButtons(promotionId);

              result["condition"] = template;
              result.status = true;
          }
        }

        return callback(result);
    });
}

exports.getPromotionDetailsById = function(channel, promotionId, callback){
    var filePath = TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }
    var result = {
            "status": false,
            "details": ""
        };

    utils.readFile(filePath , function (err, source) {
        if (err || promotionId < 0 ||
            promotionId >= promotionData.promotion.length) {
            return callback(result);
        }

        // Add sentence/questions
        var promotion = promotionData.promotion[promotionId];
        if (!promotion){
            return callback(result);
        }

        // Add button
        var template;
        var buttons = createDetailsButtons(promotionId);
        if (channel === constants.WEB_CHANNEL) {
          template = source.website_button;
          template.text = promotion.sentence;

          if (buttons.length > 0) {
              template.buttons = buttons;
              result.status = true;
          }
        } else {
            template = source.facebook_button;
            template.payload.text = promotion.sentence;

            if (buttons.length > 0) {
                template.payload.buttons = buttons;
                result.status = true;
            }
          }

        result.details = template;

        console.log("i: " + JSON.stringify(result));
        return callback (result);
    });
}


exports.getOtherByPromotionId = function(channel, promotionId, callback) {

    var otherId = getOtherIdOf(promotionId);

    getPrizeListByPromotionId (channel, otherId, function(result) {
        return callback(result);
    });
}

var getOtherIdOf = function(promotionId){
    var isExist = false;
    var promotions = promotionData.promotion;
    var otherId = "";

    for (var i = 0; i < promotions.length; i++) {
        if (promotions[i].id === promotionId) {
            isExist = true;
        } else {
            otherId = promotions[i].id;
        }
    }

    if (!isExist) {
        otherId = "";
    }

    return otherId;
}

exports.getPrizeListByPromotionId = function(channel, promotionId, callback){
    var filePath = TEMPLATE_CONFIG_PATH + "facebook_template.json";
    if (channel === constants.WEB_CHANNEL) {
      filePath = constants.TEMPLATE_CONFIG_PATH + "website_template.json";
    }
    var result = {
            "status": false,
            "template": {
                "prize": "",
                "question": ""
            }
        };

    if (!promotionId) {
        return callback(result);
    }

    utils.readFile(filePath , function (err, source) {
        // Return error when read file failed or needId is invalid
        if (err || promotionId < 0 ||
            promotionId >= promotionData.promotion.length) {
            return callback(result);
        }

        // Find prize list
        var promotion = promotionData.promotion[promotionId];

        if (!promotion){
            return callback(result);
        }

        var buttons = createPrizeButtons(promotionId, promotion.title);
        if (channel === constants.WEB_CHANNEL) {
          template = source.website_button;
          template.text = promotion.title;
          template.buttons = buttons;
        } else {
          template = source.facebook_button;
          template.payload.text = promotion.title;
          template.payload.buttons = buttons;
        }

        if (buttons.length > 0) {
            result.status = true;
            result.template.prize = promotion.prize.image_url;
            result.template.question = template;
        }

        return callback(result);
    });
};

var createTimeButtons = function(promotionId) {
    var prizeName = promotionData.promotion[promotionId].prize.name;
    return buttons = [
            {
              "type": "postback",
              "title": prizeName,
              "payload": "giai thuong " + promotionId
            },
            {
              "type": "postback",
              "title": "Điều kiện tham dự",
              "payload": "dieu kien tham du " + promotionId
            },
            {
              "type": "postback",
              "title": "Thắc mắc và liên hệ",
              "payload": "thac mac va lien he"
            }
        ];
}

var createConditionButtons = function(promotionId) {
    var prizeName = promotionData.promotion[promotionId].prize.name;
    return buttons = [
            {
              "type": "postback",
              "title": prizeName,
              "payload": "giai thuong " + promotionId
            },
            {
              "type": "postback",
              "title": "Thời gian khuyến mại",
              "payload": "thoi gian khuyen mai " + promotionId
            },
            {
              "type": "postback",
              "title": "Thắc mắc và liên hệ",
              "payload": "thac mac va lien he"
            }
        ];
}

var createButton = function(type){
    var button = {};
    if (type === "postback") {
        button = {
          "type": "postback",
          "title": "",
          "payload": ""
        }
    }

    return button;
}

var createElement = function(){
    var element = {
            "title":"",
            "item_url":"",
            "image_url":"",
            "subtitle":"",
            "buttons":[]
          }

    return element;
}

var createPrizeButtons = function(promotionId){
    var otherId = getOtherIdOf(promotionId);
    var promotionTitle = promotionData.promotion[otherId].title;

    if (promotionData.promotion.length > 1) {
        return buttons = [
            {
              "type": "postback",
              "title": "Thông tin chi tiết",
              "payload": "thong tin chi tiet " + promotionId
            },
            {
              "type": "postback",
              "title": promotionTitle,
              "payload": promotionTitle
            },
            {
              "type": "postback",
              "title": "Thắc mắc và liên hệ",
              "payload": "thac mac va lien he"
            }
        ]
    }

    return buttons = [
            {
              "type": "postback",
              "title": "Thông tin chi tiết",
              "payload": "thong tin chi tiet " + promotionId
            },
            {
              "type": "postback",
              "title": "Thắc mắc và liên hệ",
              "payload": "thac mac va lien he"
            }
        ]
}

var createDetailsButtons = function(promotionId){
    return buttons = [
            {
              "type": "postback",
              "title": "Thời gian khuyến mại",
              "payload": "thoi gian khuyen mai " + promotionId
            },
            {
              "type": "postback",
              "title": "Điều kiện tham dự",
              "payload": "dieu kien tham du " + promotionId
            }
        ]
}
