const DATABASE_CONFIG_FOLDER = global.rootPath + '/config/report/';
const LEAD_GENERATION_REPORT_TEMPLATE = "lead_generation_report_template";
const CALL_CENTER_REPORT_TEMPLATE = "call_center_report_template";
const DATE_FORMAT = "DD/MM/YYYY";
const excel = require('node-excel-export');
var mongoose = require('mongoose');
var logger = require(global.rootPath + '/controller/logger.js');
var controller = require(global.rootPath + '/controller/controller.js');
var productController = require(global.rootPath + '/controller/product_controller');
var moment = require('moment');
var utils =  require(global.rootPath + '/controller/utils/utils.js');

var Contract = require(global.rootPath + '/models/contract.model');
var Product = require(global.rootPath + '/models/product.model');
var ProductInterest = require(global.rootPath + '/models/product_interest.model');
var Promotion = require(global.rootPath + '/models/promotion.model');
var PromotionInterest = require(global.rootPath + '/models/promotion_interest.model');
var Question = require(global.rootPath + '/models/question.model');
var User = require(global.rootPath + '/models/user.model');

exports.reportLeadGeneration = function(reportTime, callback){
	logger.debug("Start report lead generation.");

	var today = moment().format(DATE_FORMAT);
	var yesterday = moment().subtract(1, "days").format(DATE_FORMAT);
	logger.debug("Report Lead Generation: " + yesterday + "_" + today);

	// Query data from Product Interest
	ProductInterest.find({$or: [{ 'create_date': ''+yesterday }, { 'create_date': ''+today }]}).populate('_user').exec()
	.then(function(productInterests) {
		logger.debug("Number of data: " + productInterests.length);

		var data = createProductInterestRecords(reportTime, productInterests);
		logger.debug("Data: " + data.length);

  	// Create excel
  	var excelFile = createLeadGenerationExcel(data, today);

		controller.sendExcelToEmail(LEAD_GENERATION_REPORT_TEMPLATE, excelFile, function(result){
    		logger.debug("Result: " + JSON.stringify(result));
    		return callback(result);
    	});

	}).catch(function(err) {
  		return callback(err)
  	});
}

exports.reportCallCenter = function(reportTime, callback){
	// Query data from Product Interest
	var today = moment().format(DATE_FORMAT);
	var yesterday = moment().subtract(1, "days").format(DATE_FORMAT);

	logger.debug("Call Center Report: " + yesterday + "_" + today);
	Question.find({$or: [{ 'create_date': ''+yesterday }, { 'create_date': ''+today }]}).populate('_user').exec()
	.then(function(questionData) {
		// logger.debug("Number of data: " + questionData.length);

		var data = createQuestionRecords(reportTime, questionData);
	    logger.debug("Content of data: " + data);
    	// Create excel
    	var excelFile = createCallCenterExcel(data, today);

	    controller.sendExcelToEmail(CALL_CENTER_REPORT_TEMPLATE, excelFile, function(result){
	    	logger.debug("Result: " + JSON.stringify(result));
	    	return callback(result);
	    });

	}).catch(function(err) {
  		return callback(err)
  	});
}

// Create product interest records
var createProductInterestRecords = function(reportTime, productInterestList) {
	var data = [];
	var today = moment().format(DATE_FORMAT);
	var todayObj = moment();
	todayObj = moment(todayObj, DATE_FORMAT);
	var yesterday = moment().subtract(1, "days").format(DATE_FORMAT);
	for (var i = 0; i < productInterestList.length; i++) {
		var productInterest = productInterestList[i];
		var createTime = productInterest.create_time;
		var createDate = productInterest.create_date;
		var appointmentDate = productInterest.appointment_date;
		logger.debug("Create Date: " + createDate);
		logger.debug("Create Time: " + createTime);
		logger.debug("Appointment Date: " + appointmentDate);
		// var momentAppointmentDate = moment(appointmentDate, DATE_FORMAT);
		// logger.debug("Appointment Date 2: " + momentAppointmentDate);
		//
		// momentAppointmentDate.isAfter(todayObj)
		var index = 0;
		if ((createDate === today) ||
		   ((createDate === yesterday) && (createTime >= reportTime))){
			var user = productInterest._user[0];
			let record = {};

			// Add data related to user
			index = index + 1;
			record['number'] = index;
			record['full_name'] = utils.decrypt(user['full_name']);
			record['birthday'] = utils.decrypt(user['birthday']);
			record['gender'] = utils.decrypt(user['gender']);
			record['phone'] = utils.decrypt(user['phone']);
			record['location'] = utils.decrypt(user['location']);
			record['income'] = utils.decrypt(user['income']);
			// Add data related to product
			record['create_date'] = productInterest['create_date'];
			record['create_time'] = productInterest['create_time'];
			record['appointment_date'] = productInterest['appointment_date'];
			record['appointment_time'] = productInterest['appointment_time'];
			// Update product name
			var productId = utils.decrypt(productInterest['product_id']);
			productController.getProductNameById(productId, function(result){
				record['product_interest'] = result.product_name;
			});

			logger.debug("Record " + i + ": " + JSON.stringify(record));

			data.push(record);
		}
	}
	// logger.debug("Data : " + data);
	return data;
};

// Create product interest records
var createQuestionRecords = function(reportTime, questionList) {
	var data = [];
	var today = moment().format(DATE_FORMAT);
	var yesterday = moment().subtract(1, "days").format(DATE_FORMAT);
	var index = 0;
	for (var i = 0; i<questionList.length; i++) {
		var questionData = questionList[i];
		var createTime = questionData.create_time;
		var createDate = questionData.create_date;

		// report records from yesterday's report time to today.
		if ((createDate === today) ||
		   ((createDate === yesterday) && (createTime >= reportTime))) {
			var user = questionData._user[0];
			let record = {};

			// Add data related to user
			index = index + 1;
			record['number'] = index;

			if (user['full_name']) {
				record['full_name'] = utils.decrypt(user['full_name']);
			}

			if (user['contract_number']) {
				record['contract_number'] = utils.decrypt(user['contract_number']);
			}

			if (user['phone']) {
				record['phone'] = utils.decrypt(user['phone']);
			}

			// Add data related to product
			if (questionData['question_flow']) {
				record['question_flow'] = utils.decrypt(questionData['question_flow']);
			}

			if (questionData['question_content']) {
				record['question_content'] = utils.decrypt(questionData['question_content']);
			}

			if (questionData['create_time']) {
				record['create_time'] = questionData['create_time'];
			}

			logger.debug("Record: " + JSON.stringify(record));

			data.push(record);
		}

	}
	// logger.debug("Data : " + data);
	return data;
};

// Configuration for excel file
var createSpecification = function(json){
	let specification = {} ;
	var obj = json;
	var keys = Object.keys(obj);

	for (var i = 0; i < keys.length; i++) {
	  var key = keys[i];
		var object = obj[key];
	  let jsonObject = {};
	  jsonObject.displayName = object.name;
		jsonObject.width = object.width;
		jsonObject.headerStyle = styles.TitleDB;
		jsonObject.cellStyle = styles.Cell_Top;
		if (object.align === "center") {
				jsonObject.cellStyle = styles.Cell_Center;
		}

		jsonObject.cellFormat = function(value, row) {
      return (!value) ? '' : value;
    };

	  specification[key] = jsonObject;
	}

	return specification;
}

var createLeadGenerationExcel = function(data, today){
	// logger.debug("Step 0");
	var dataJson = require(DATABASE_CONFIG_FOLDER + 'lead_generation.json');

	// Create specification, heading, merge
	let specification = createSpecification(dataJson.data);

	const heading = [
	  [{value: dataJson.report_name, style: styles.Title}],[{value: 'Date : ' + today, style: styles.Date}]
	];

	const merges = [
	  { start: { row: 1, column: 1 }, end: { row: 1, column: 12 }},{ start: { row: 2, column: 1 }, end: { row: 2, column: 12 } }
	];

	// Create report
	var report = excel.buildExport(
	  [
	    {
	      name: dataJson.report_name,
	      heading: heading,
	      merges: merges,
	      specification: specification,
	      data: data
	    }
	  ]
	);

	fileName = dataJson.report_name + "_" + today.replace(/\//g, "_")

	var result = {
		"name": fileName,
		"data": report
	}

	return result;
}

var createCallCenterExcel = function(data, today){
	logger.debug("Step 0");
	var dataJson = require(DATABASE_CONFIG_FOLDER + 'call_center.json');

	// Create specification, heading, merge
	let specification = createSpecification(dataJson.data);
	logger.debug("After create specification");
	const heading = [
	  [{value: dataJson.report_name, style: styles.Title}],[{value: 'Date : ' + today, style: styles.Date}]
	];

	const merges = [
	  { start: { row: 1, column: 1 }, end: { row: 1, column: 10 }},{ start: { row: 2, column: 1 }, end: { row: 2, column: 10 } }
	];

	// Create report
	var report = excel.buildExport(
	  [
	    {
	      name: dataJson.report_name,
	      heading: heading,
	      merges: merges,
	      specification: specification,
	      data: data
	    }
	  ]
	);

	fileName = dataJson.report_name + "_" + today.replace(/\//g, "_")
	logger.debug("After create fileName");
	var result = {
		"name": fileName,
		"data": report
	}

	return result;
}

const styles = {
    Title: {
      alignment:{
        vertical: 'center',
        horizontal:'center'
      },
      font: {
        name:"Calibri",
        sz: 36,
        bold: true,
      }
    },
    Date: {
      alignment:{
        vertical: 'center',
        horizontal:'center'
      },
      font: {
        name:"Calibri",
        sz: 16
      }
    },
		Cell_Center: {
			alignment:{
        vertical: 'center',
        horizontal:'center',
				wrapText: true
      },
      font: {
        name:"Calibri",
        sz: 12
      }
		},
    Cell_Top: {
      alignment:{
        vertical: 'center',
        horizontal:'top',
				wrapText: true
      },
      font: {
        name:"Calibri",
        sz: 12
      }
    },
    TitleDB: {
      fill :{
        fgColor: {
          rgb: 'c65911'
        }
      },
      alignment:{
        vertical: 'center',
        horizontal:'center',
				wrapText: true
      },
      font: {
        name:"Arial",
        sz: 10,
        bold: true,
        color: { rgb: "ffffff" }
      },
      border: {
      	top: { style: 'thin', color: { rgb: "cccccc" } },
      	bottom: { style: 'thin', color: { rgb: "cccccc" } },
      	left: { style: 'thin', color: { rgb: "cccccc" } },
      	right: { style: 'thin', color: { rgb: "cccccc" } }
      }
    }
  };
