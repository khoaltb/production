const TEMPLATE_CONFIG_PATH = global.rootPath + '/config/template/';
const WEB_CHANNEL = "WEB";
const FB_CHANNEL = "FB";

module.exports = {
    TEMPLATE_CONFIG_PATH: TEMPLATE_CONFIG_PATH,
    WEB_CHANNEL: WEB_CHANNEL,
    FACEBOOK_CHANNEL: FB_CHANNEL
};
