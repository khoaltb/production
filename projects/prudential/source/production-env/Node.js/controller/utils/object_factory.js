var logger = require(global.rootPath + '/controller/logger.js');
var nodemailer = require('nodemailer');
var SelfReloadJSON = require('self-reload-json');
var emailAuth = new SelfReloadJSON(global.rootPath + '/config/email/email_auth.json');

// Create mail object when user ask about other products
exports.createMailObject = function(mailObject, contentObject) {

  logger.debug("Mail Object: " + JSON.stringify(mailObject));

  var json = mailObject;
  json.to = contentObject.email.to;
  json.cc = contentObject.email.cc;
  var text = json.html;

  logger.debug("Mail Object From JSON: " + JSON.stringify(mailObject));
  //////////////////////////////// Update User Info ///////////////////////////////////
  if (contentObject.user) {
    var user = contentObject.user;

    text = text.replace("customer_name", user.full_name);

    if (user.contract_number) {
      text = text.replace("contract_number", user.contract_number);
    }

    if (user.phone) {
      text = text.replace("customer_phone", user.phone);
    }

    if (user.facebook_name) {
      text = text.replace("facebook_name", user.facebook_name);
    }
  }

  //////////////////////////////// Update Appointment Info ///////////////////////////////////
  if (contentObject.appointment_time) {
    text = text.replace("appointment_time", contentObject.appointment_time);
  }

  // Update appointment date
  if (contentObject.appointment_date) {
    text = text.replace("appointment_date", contentObject.appointment_date);
  }

  //////////////////////////////// Update Question Info ///////////////////////////////////
  if (contentObject.question) {
    var question = contentObject.question;

    if (question.question_content) {
      text = text.replace("customer_question", question.question_content);
    }

    if (question.question_flow) {
      json.subject = json.subject.replace("customer_problem", question.question_flow);
      text = text.replace("customer_problem", question.question_flow);
    }
  }

  //////////////////////////////// Update Product Info ///////////////////////////////////
  if (contentObject.product_name) {
    text = text.replace("product_name", contentObject.product_name);
  }

  json.html = text;
  json.attachments = contentObject.attachments;
  logger.debug("Mail Object After Edit: " + JSON.stringify(json));

  return json;
}

exports.createTransporter = function() {

  // // For Gmail
  // var transporter = nodemailer.createTransport({
  //   service: emailAuth.service,
  //   auth: emailAuth.auth
  // });

  // For other SMTP
  var transporter = nodemailer.createTransport({
    host: emailAuth.host,
    port: emailAuth.port,
    secure: emailAuth.secure,
    auth: emailAuth.auth
  });

  return transporter;
}
