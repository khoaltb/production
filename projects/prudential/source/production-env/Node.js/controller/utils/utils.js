/**
* utils.js:
* - holds functions regarding to calculation, read file or check empty object
* - ...
*/
var logger = require(global.rootPath + '/controller/logger.js');
var SelfReloadJSON = require('self-reload-json');
var secret = new SelfReloadJSON(global.rootPath + '/config/environment/auth_key.json');
var constants = require(global.rootPath + '/controller/utils/constants.js');
var CryptoJS = require("crypto-js");

exports.readFile = function(filePath , callbackReadFile){
  var fs = require('fs');
  fs.readFile(filePath, function (err,data) {
    if (err) {
      logger.debug(err);
    }
    callbackReadFile(err, JSON.parse(data));
  });
}

exports.isEmptyObject = function(obj) {
  return !Object.keys(obj).length;
}

// Create regex with form ^(abc|def)$
exports.createRegex = function(arr) {
	var regex = "";

	if (arr) {
		regex = "^(";

		for (var i=0; i<arr.length; i++) {
			regex += arr[i];
			if (i < arr.length-1) {
				regex += "|";
			}
		}

		regex += ")$";
	}

	return regex;
}

exports.createButton = function(type, channel){
    var button = {};
    if (type === "postback") {
        button = {
          "type": "postback",
          "title": "",
          "payload": ""
        }
    } else if (type === "quick_reply") {
      button = {
        "content_type": "text",
        "title": "",
        "payload": ""
      }

      if (channel === constants.WEB_CHANNEL) {
        button = {
          "type": "postback",
          "title": "",
          "payload": ""
        }
      }
    }

    return button;
}

exports.encrypt = function(plainText) {
  var ciphertext = CryptoJS.AES.encrypt(plainText, secret.database_auth_key);

  return ciphertext.toString();
}

exports.decrypt = function(ciphertext) {
  var bytes  = CryptoJS.AES.decrypt(ciphertext, secret.database_auth_key);
  var plainText = bytes.toString(CryptoJS.enc.Utf8);

  return plainText;
}
