/**
* index.js:
* - main file running
* - holds RESTFUL apis such as: send Calendar, validate...
*/
var express = require('express');
var app = express();

const path = require('path');
const rootPath = path.dirname(require.main.filename);

global.rootPath = rootPath;
var logger = require(global.rootPath + '/controller/logger.js');
var controller = require(global.rootPath + '/controller/controller');
var productController = require(global.rootPath + '/controller/product_controller');
var mapController = require(global.rootPath + '/controller/map_controller');
var promotionController = require(global.rootPath + '/controller/promotion_controller');
var databaseController = require(global.rootPath + '/controller/database_controller');
var reportController = require(global.rootPath + '/controller/report_controller');
var validator = require(global.rootPath + '/controller/validator');
var utils = require(global.rootPath + '/controller/utils/utils');
var SelfReloadJSON = require('self-reload-json');
var secret = new SelfReloadJSON(global.rootPath + '/config/environment/auth_key.json');
global.AUTH_KEY = secret.api_auth_key;

// Connect to MongoDB
var config = require(global.rootPath + '/config/environment/database_info');
var mongoose = require('mongoose');
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function(err) {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(-1); // eslint-disable-line no-process-exit
});

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.set('port', (process.env.PORT || 8080));

app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');

app.get('/web',
  function(req, res){
    res.sendFile(path.resolve('views/index.html'));
  });

app.get('/', function(req, res) {
  res.send("Server runs successfully!");
});

var utils =  require(global.rootPath + '/controller/utils/utils.js');
app.post('/test', function(req, res){
  // var today = new Date();
  // var date = moment().format("DD/MM/YYYY");;

  var result = {
      "status": true
    };
  // res.send(result);

  var time = req.body.time;
  if (req.body.type === "call_center") {

    reportController.reportCallCenter(time, function(output){
      logger.debug("Result: " + output.status);
      res.send(result);
    });

  } else {
    // Save user
    reportController.reportLeadGeneration(time, function(output){
      logger.debug("Result: " + output.status);
      res.send(result);
    });
  }
});

app.get('/reportCallCenter',
  function(req, res){
    var reportTime = req.query.report_time;
    if (!reportTime) {
      reportTime = "16:00";
    }
    reportController.reportCallCenter(reportTime, function(output){
      logger.debug("Result: " + output.status);
      res.send(output);
    });
  });

app.get('/reportLeadGeneration',
    function(req, res){
      var reportTime = req.query.report_time;
      if (!reportTime) {
        reportTime = "16:00";
      }
      reportController.reportLeadGeneration(reportTime, function(output){
        logger.debug("Result: " + output.status);
        res.send(output);
      });
    });

app.post('/saveQuestion', function(req, res){
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  // Save user
  databaseController.saveQuestion(req, function(result){
    res.send(result);
  });

});

app.post('/saveProductInterest', function(req, res){
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  // Save user
  databaseController.saveProductInterest(req, function(result){
    res.send(result);
  });

});

app.post('/notifyCallCenter', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  logger.debug("Content of Request: " + JSON.stringify(req.body));
  // Save database
  databaseController.saveQuestion(req, function(saveDBresult){
    if (!saveDBresult.status) {
      res.send(saveDBresult);
    }

    // Send email
    var emailObject = req.body;
    logger.debug(JSON.stringify(emailObject));
    controller.sendEmail(emailObject, function(sendEmailResult) {
        console.log("End of send email");
        res.send(sendEmailResult);
    });
  });

});

app.post('/notifyLeadGeneration', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  // Save database
  databaseController.saveProductInterest(req, function(saveDBresult){
    if (!saveDBresult.status) {
      res.send(saveDBresult);
    }

    // Send email
    var emailObject = req.body;
    logger.debug(JSON.stringify(emailObject));
    controller.sendEmail(emailObject, function(sendEmailResult) {
        console.log("End of send email");
        res.send(sendEmailResult);
    });
  });

});

app.post('/sendEmail', function(req, res) {
	// Authorize request by auth_key
	var authKey = req.headers['auth_key'];
	if (authKey != AUTH_KEY) {
		return res.send({});
	}

	// Send email
	var emailObject = req.body;
  logger.debug(JSON.stringify(emailObject));
	controller.sendEmail(emailObject, function(result) {
	    res.send(result);
	});
});

app.post('/getNext5Days', function(req, res){
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var question = req.body.question;
  var channel =  req.body.channel;
  controller.getNext5Days(channel, question, function(result) {
      res.send(result);
  });

});

/**
* ----------------------------------------------------------------------
* Validate email, phone, date by locale
* @param: date, phone, email
* @return result in Json object
*/

app.post('/validate', function(req, res) {
  logger.debug("Check \: ");
	// Authorize request by auth_key
	var authKey = req.headers['auth_key'];
	if (authKey != AUTH_KEY) {
		return res.send({});
	}

	var type = req.body.type;
	var data = req.body.data;
	logger.debug("Data: " + data);
	validator.validate(type, data, function callback(result) {
		res.send(result);
	});
});

app.post('/getNeeds', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  productController.getNeeds(req.body.channel, function(result){
    res.send(result);
  });

});

app.post('/getNeedById', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var needId = req.body.need_id;
  var channel = req.body.channel;

  console.log("Need id: " + needId);
  productController.getDataById(channel, needId, "", function(result){
    res.send(result);
  });

});

app.post('/getProductsByTypeId', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var typeId = req.body.type_id;
  var channel = req.body.channel;
  productController.getProductsByTypeId(channel, typeId, function(result){
    res.send(result);
  });

});

app.post('/getProductDetailsById', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var inputId = req.body.input_id;
  productController.getProductDetailsById(inputId, function(result){
    res.send(result);
  });
});

app.post('/getOthersByProductId', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var inputId = req.body.input_id;
  var channel = req.body.channel;
  productController.getOthersByProductId(channel, inputId, function(result){
    res.send(result);
  });
});

app.post('/getProductNameById', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var inputId = req.body.input_id;
  productController.getProductNameById(inputId, function(result){
    res.send(result);
  });
});

app.post('/getPromotions', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  promotionController.getPromotions(req.body.channel, function(result){
    res.send(result);
  });
});

app.post('/getPrizeListByPromotionId', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var promotionId = req.body.promotion_id;
  var channel = req.body.channel;
  promotionController.getPrizeListByPromotionId(channel, promotionId, function(result){
    res.send(result);
  });
});

app.post('/getOtherByPromotionId', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var promotionId = req.body.promotion_id;
  promotionController.getOtherByPromotionId(promotionId, function(result){
    res.send(result);
  });
});

app.post('/getPrizeListByPromotionIdTemp', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var promotionId = req.body.promotion_id;
  promotionController.getPrizeListByPromotionIdTemp(promotionId, function(result){
    res.send(result);
  });
});

app.post('/getPromotionDetailsById', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var promotionId = req.body.promotion_id;
  var channel = req.body.channel;
  promotionController.getPromotionDetailsById(channel, promotionId, function(result){
    res.send(result);
  });
});

app.post('/getDataByPromotionId', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  var promotionId = req.body.promotion_id;
  var type = req.body.type;
  var channel = req.body.channel;
  promotionController.getDataByPromotionId(channel, promotionId, type, function(result){
    res.send(result);
  });
});

app.post('/getCity', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }

  mapController.getCity(req, function(result){
    res.send(result);
  });
});

app.post('/updateEmailReport', function(req, res) {
  // Authorize request by auth_key
  var authKey = req.headers['auth_key'];
  if (authKey != AUTH_KEY) {
    return res.send({});
  }


  controller.updateEmailReport(req, function(result){
    res.send(result);
  });
});

app.listen(app.get('port'), function() {
  logger.debug('Node app is running on port', app.get('port'));
});
