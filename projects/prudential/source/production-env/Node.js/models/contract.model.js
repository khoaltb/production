var mongoose = require('mongoose');

var ContractSchema = new mongoose.Schema({
    // id: String,
    contract_id: String,
    user_id: String
});

module.exports = mongoose.model('Contract', ContractSchema);