var moment = require('moment');
var logger = require(global.rootPath + '/controller/logger.js');
var utils =  require(global.rootPath + '/controller/utils/utils.js');

exports.createUserData = function(user) {
  logger.debug("Enrypt 1");
  let userData = {};

  if (user.uuid) {
    userData['uuid'] = user.uuid;
  }

  if (user.full_name) {
    userData['full_name'] = utils.encrypt(user.full_name);
  }

  if (user.phone) {
    userData['phone'] = utils.encrypt(user.phone);
  }

  if (user.birthday) {
    userData['birthday'] = utils.encrypt(user.birthday);
  }

  if (user.gender) {
    userData['gender'] = utils.encrypt(user.gender);
  }

  if (user.location) {
    userData['location'] = utils.encrypt(user.location);
  }

  if (user.income) {
    userData['income'] = utils.encrypt(user.income);
  }

  if (user.contract_number) {
    userData['contract_number'] = utils.encrypt(user.contract_number);
  }

  return userData;
}

exports.createProductInterestData = function(userId, data){
  var today = moment().format("DD/MM/YYYY");
  var time = moment().format("HH:mm");
  console.log("User_Id: " + today);
  return {
    "_user": userId,
    "product_id": utils.encrypt(data.product_id),
    "appointment_date": data.appointment_date,
    "appointment_time": data.appointment_time,
    "create_date": today,
    "create_time": time
  }
}

exports.createQuestionData = function(userId, data){
  var today = moment().format("DD/MM/YYYY");
  var time = moment().format("HH:mm");
  console.log("User_Id: " + today);

  return {
    "_user": userId,
    "question_content": utils.encrypt(data.question_content),
    "question_flow": utils.encrypt(data.question_flow),
    "create_date": today,
    "create_time": time
  }
}
