var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    // id: String,
    product_name: String,
    subtitle: String,
    image_url: String,
    details: String


});

module.exports = mongoose.model('Product', ProductSchema);