var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductInterestSchema = new mongoose.Schema({
    // id: String,
    product_id: String,
    _user: [{ type: Schema.ObjectId, ref: 'User' }],
    appointment_date: String,
    appointment_time: String,
    create_date: String,
    create_time: String
});

module.exports = mongoose.model('ProductInterest', ProductInterestSchema);