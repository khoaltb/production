var mongoose = require('mongoose');

var PromotionSchema = new mongoose.Schema({
    // id: String,
    promotion_name: String,
    promotion_type: String
});

module.exports = mongoose.model('Promotion', PromotionSchema);