var mongoose = require('mongoose');

var PromotionInterestSchema = new mongoose.Schema({
    // id: String,
    promotion_id: String,
    user_id: String
});

module.exports = mongoose.model('PromotionInterest', PromotionInterestSchema);