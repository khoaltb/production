var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = new mongoose.Schema({
    // id: String,
    _user: [{ type: Schema.ObjectId, ref: 'User' }],
    create_date: String,
    create_time: String,
    question_content: String,
    question_flow: String

});

module.exports = mongoose.model('Question', QuestionSchema);