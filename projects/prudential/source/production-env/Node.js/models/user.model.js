var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    // id: String,
    uuid: String,
    full_name: String,
    first_name: String,
    middle_name: String,
    last_name: String,
    nick_name: String,
    contract_number: String,
    profile_pic: String,
    age: String,
    birthday: String,
    phone: String,
    email: String,
    relationship_status: String,
    work: String,
    locale_language: String,
    locale_country: String,
    location: String,
    timezone: String,
    gender: String,
    first_referral: String,
    last_referral: String,
    income: String
});

module.exports = mongoose.model('User', UserSchema);